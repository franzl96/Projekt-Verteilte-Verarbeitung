package presentation;

public enum EnvironmentVariables {

    SENSOR_ID("SensorId"),
    SMART_HOME_SERVICE_REGISTRATION_URL("SmartHomeServiceRegistrationURL"),
    SMART_HOME_SERVICE_PUBLISH_URL("SmartHomePublishURL");

    private final String name;

    EnvironmentVariables(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getValue(){
        return System.getenv(name);
    }
}
