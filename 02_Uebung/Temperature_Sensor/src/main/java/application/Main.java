package application;

import application.exceptions.EnvironmentVariablesNotFoundException;
import application.exceptions.RegistrationException;
import application.exceptions.SendException;
import infrastructure.HttpMessageServiceImpl;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.SecureRandom;
import java.util.concurrent.TimeUnit;

public class Main {

    private static final Logger LOGGER = LogManager.getLogger(Main.class.getName());

    public static void main(String[] args) {
        LOGGER.log(Level.INFO, "Sensor stating...");

        try{
            IHttpMessageService measurementSender = new HttpMessageServiceImpl();
            measurementSender.registerOnSmartHomeService();
            LOGGER.log(Level.INFO, "Sensor registration successful!");

            startMeasurementLoop(measurementSender);
        } catch (EnvironmentVariablesNotFoundException e) {
            LOGGER.error("A environment variable is missing!");
            LOGGER.info("Shutdown Sensor");
            System.exit(1);
        } catch (RegistrationException e) {
            LOGGER.error(e.getMessage());
            LOGGER.info("Shutdown Sensor");
            System.exit(1);
        }
    }


    private static void startMeasurementLoop(IHttpMessageService measurementSender){
        LOGGER.info("Start Measurement Loop");
        while (!Thread.currentThread().isInterrupted()){
            try {
                TimeUnit.SECONDS.sleep(60);
                int randomNr = new SecureRandom().nextInt(31) - 1;
                measurementSender.sendMeasurementData(randomNr);

            } catch (InterruptedException e) {
                LOGGER.error(e.getMessage());
                Thread.currentThread().interrupt();
                System.exit(1);
            } catch (SendException e) {
                LOGGER.error(e.getMessage());
            }
        }
    }
}
