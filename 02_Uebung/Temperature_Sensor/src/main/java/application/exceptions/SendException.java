package application.exceptions;

public class SendException extends Exception{
    public SendException(String msg, Throwable inner){
        super(msg, inner);
    }
}
