package application;

import application.exceptions.RegistrationException;
import application.exceptions.SendException;

public interface IHttpMessageService {

    void registerOnSmartHomeService() throws RegistrationException;
    void sendMeasurementData(Integer value) throws SendException;

}
