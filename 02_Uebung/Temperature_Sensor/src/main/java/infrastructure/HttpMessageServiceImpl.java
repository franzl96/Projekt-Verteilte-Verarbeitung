package infrastructure;

import application.exceptions.RegistrationException;
import application.exceptions.SendException;
import presentation.EnvironmentVariables;
import domain.HttpStatusCodes;
import application.IHttpMessageService;
import application.exceptions.EnvironmentVariablesNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDateTime;

public class HttpMessageServiceImpl implements IHttpMessageService {

    private static final Logger LOGGER = LogManager.getLogger(HttpMessageServiceImpl.class.getName());
    private static final String DELIMITTER = "/";

    public HttpMessageServiceImpl() throws EnvironmentVariablesNotFoundException {
        for(EnvironmentVariables environmentVariables: EnvironmentVariables.values()){
            if(environmentVariables.getValue() == null)
                throw new EnvironmentVariablesNotFoundException();
        }
    }

    @Override
    public void registerOnSmartHomeService() throws RegistrationException {
        try {
            String pathString = EnvironmentVariables.SMART_HOME_SERVICE_REGISTRATION_URL.getValue();
            URI uri = new URI(pathString);
            StringBuilder body = new StringBuilder()
                    .append("{\"id\":")
                    .append(EnvironmentVariables.SENSOR_ID.getValue())
                    .append(",\"sensorName\":\"Tempsensor\",\"location\":\"Schlafzimmer\"}");

            String logMSG = "Body to Send: " + body.toString();
            LOGGER.info(logMSG);
            int code = httpPOST(uri, body.toString());
            checkResponseCode(code);

        } catch (URISyntaxException e) {
            throw new RegistrationException("The given url for the SMART_HOME_SERVICE_REGISTRATION_URL is invalid", e);
        } catch (IOException e) {
            throw new RegistrationException(e.getMessage(), e);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RegistrationException("The application is interrupted", e);
        }
    }

    @Override
    public void sendMeasurementData(Integer value) throws SendException {
        try {
            String pathString = EnvironmentVariables.SMART_HOME_SERVICE_PUBLISH_URL.getValue() + DELIMITTER + EnvironmentVariables.SENSOR_ID.getValue();
            URI uri = new URI(pathString);
            StringBuilder body = new StringBuilder()
                    .append("{\"timestamp\":\"")
                    .append(LocalDateTime.now().toString())
                    .append("\",\"currentValue\":")
                    .append(value)
                    .append(",\"unit\":\"Celsius\"}");

            String logMSG = "Body to Send: " + body.toString();
            LOGGER.info(logMSG);

            int code = httpPOST(uri, body.toString());
            checkResponseCode(code);

        } catch (URISyntaxException e) {
            throw new SendException("The given url for the SMART_HOME_SERVICE_REGISTRATION_URL is invalid", e);
        } catch (IOException e) {
            throw new SendException(e.getMessage(), e);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new SendException("The application is interrupted", e);
        }
    }

    private int httpPOST(URI uri, String body) throws IOException, InterruptedException {
        HttpRequest httpRequest = HttpRequest.newBuilder(uri)
                .setHeader("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .build();

        return sendRequest(httpRequest);

    }

    private int sendRequest(HttpRequest httpRequest) throws IOException, InterruptedException {
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        return response.statusCode();
    }

    private void checkResponseCode(Integer code){
        HttpStatusCodes httpStatusCodes = HttpStatusCodes.getByCode(code);
        String logMSG = "Result: " + httpStatusCodes.toString();
        LOGGER.info(logMSG);
    }



}
