#!/bin/bash
# waitForService.sh

host=$1
timeout=$2
timeoutcount=5

code=$(curl -o /dev/null -s -w "%{http_code}\n" "$host")

echo $code

until [ $code = 200 ]; do
  echo "Service is not available -> sleeping"
  sleep 5

  if [ $timeoutcount -ge $timeout ]
  then
    exit
  fi

  timeoutcount=$[$timeoutcount + 5]
  code=$(curl -o /dev/null -s -w "%{http_code}\n" "$host")


  done

  echo "Service is now available -> starting"

  exec $3