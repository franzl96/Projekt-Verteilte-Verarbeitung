package infrastructure;

import application.common.exceptions.ContentNotAvailableException;
import application.common.exceptions.ContentVersionException;
import application.datamanager.ADataManager;
import application.datamanager.ActorDataManager;
import application.interfaces.proxyservice.IProxy;
import domain.*;
import infrastructure.exceptions.AuthenticationException;
import infrastructure.exceptions.DataFetchException;
import infrastructure.exceptions.LogicRuleException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import persistance.ActorDBRepo;
import persistance.RuleDBRepo;
import persistance.SensorDBRepo;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Service
public class LogicRuleEngine {

    private final RuleDBRepo ruleDBRepo;
    private final SensorDBRepo sensorDBRepo;
    private final ActorDBRepo actorDBRepo;
    private final IProxy proxy;
    private ADataManager<Actor> actorDataManager;

    private final Logger logger = LogManager.getLogger(LogicRuleEngine.class.getName());

    @Autowired
    public LogicRuleEngine(IProxy proxy, RuleDBRepo ruleDBRepo, SensorDBRepo sensorDBRepo, ActorDBRepo actorDBRepo) {
        this.proxy = proxy;
        this.ruleDBRepo = ruleDBRepo;
        this.sensorDBRepo = sensorDBRepo;
        this.actorDBRepo = actorDBRepo;
    }

    @PostConstruct
    public void init() {
        this.actorDataManager = new ActorDataManager(actorDBRepo);
    }

    public void startLogicRuleEngine() {
        logger.info("The logic rule engine is started!");
        while (!Thread.currentThread().isInterrupted()) {
            extractSensorData();
            try {
                TimeUnit.SECONDS.sleep(10);
            } catch (InterruptedException e) {
                logger.error("The logic rule engine is interrupted");
                Thread.currentThread().interrupt();
            }
        }
    }

    private void extractSensorData() {
        logger.info("Check Data");
        List<Rule> rules = new ArrayList<>();
        ruleDBRepo.findAll().forEach(rules::add);

        for (Rule rule : rules) {
            String msg = "Check Rule: " + rule.getRuleName();
            logger.info(msg);
            Optional<Sensor> sensor = sensorDBRepo.findById(rule.getSensorID());
            if (sensor.isEmpty()) return;
            try {
                checkSensorData(sensor.get().getSensorDataList(), rule);
            } catch (LogicRuleException e) {
                logger.error(e.getMessage());
            }
        }
    }

    /**
     * This methode checks the sensordata and the weather data and react
     *
     * @param sensorDataList list of sensor data-> FetchType.EAGER required!
     * @param rule           the rule that triggered
     */
    private void checkSensorData(List<SensorData> sensorDataList, Rule rule) throws LogicRuleException {
        if (sensorDataList.isEmpty()) return;
        SensorData lastMeasuredObject = sensorDataList.get(sensorDataList.size() - 1);

        Optional<Actor> actor = actorDBRepo.findById(rule.getActorID());

        if (actor.isEmpty()) return;

        boolean isSunny = isSunny();

        String msg = "Sensor: " + lastMeasuredObject.getSensorID();
        logger.info(msg);
        msg = "Threshold: " + rule.getThreshold() + ", Current Value: " + lastMeasuredObject.getCurrentValue();
        logger.info(msg);
        msg = "ForecastService: IsSunny: " + isSunny;
        logger.info(msg);

        if (lastMeasuredObject.getCurrentValue() < rule.getThreshold() || !isSunny) {
            logger.info("Shutter closed");
            setActor(actor.get(), "closed");
        } else if (lastMeasuredObject.getCurrentValue() > rule.getThreshold()) {
            logger.info("Shutter opened");
            setActor(actor.get(), "open");
        }
    }

    private boolean isSunny() throws LogicRuleException {
        return checkOnlineForecastDatabase().getSummary().equals("Sunny");
    }

    private ForecastData checkOnlineForecastDatabase() throws LogicRuleException {
        ForecastData forecastData;

        try {
            forecastData = proxy.getWeatherData();
            return forecastData;
        } catch (AuthenticationException e) {
            throw new LogicRuleException("Invalid Auth-Data! - HttpErrorCode: " + e.getLocalizedMessage(), e);
        } catch (DataFetchException e) {
            throw new LogicRuleException("Error while loading data - HttpErrorCode: " + e.getLocalizedMessage(), e);
        }
    }

    private void setActor(Actor actor, String value) throws LogicRuleException {
        if (actor.getStatus() != null && actor.getStatus().equals(value)) return;
        actor.setStatus(value);
        try {
            int code = proxy.sendNewStatusToActor(actor.getServiceURL(), value);

            if (code == 400) {
                throw new LogicRuleException("The state Object is not valid");
            } else if (code != 200 && code != 202 && code != 201) {
                throw new LogicRuleException("A common http error was happened! Code: " + code);
            }

            //Change the value in the database if the operation was successful!
            actorDataManager.updateSingleObject(actor.getID(), actor);
        } catch (ContentNotAvailableException | ContentVersionException e) {
            throw new LogicRuleException("A error on saving in database happened!", e);
        }
    }
}
