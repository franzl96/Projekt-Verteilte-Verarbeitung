package infrastructure.exceptions;

public class DataFetchException extends Exception {

    public DataFetchException() {
        super();
    }

    public DataFetchException(String msg) {
        super(msg);
    }

    public DataFetchException(Throwable exception) {
        super(exception);
    }
}
