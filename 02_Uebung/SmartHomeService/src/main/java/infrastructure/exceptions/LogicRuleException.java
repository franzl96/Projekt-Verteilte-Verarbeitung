package infrastructure.exceptions;

public class LogicRuleException extends Exception {

    public LogicRuleException(String msg) {
        super(msg);
    }

    public LogicRuleException(String msg, Throwable inner) {
        super(msg, inner);
    }
}
