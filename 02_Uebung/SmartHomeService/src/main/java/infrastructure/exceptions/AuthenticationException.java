package infrastructure.exceptions;

public class AuthenticationException extends Exception {

    public AuthenticationException(String msg) {
        super(msg);
    }

    public AuthenticationException(Throwable exception) {
        super(exception);
    }
}

