package infrastructure.proxyservice;

import application.common.EnvironmentVariables;
import application.interfaces.proxyservice.IWeatherForecast;
import domain.ForecastData;
import infrastructure.exceptions.AuthenticationException;
import infrastructure.exceptions.DataFetchException;
import nonapi.io.github.classgraph.json.JSONDeserializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
public class WeatherForecastServiceImpl implements IWeatherForecast {

    private static final String AUTH_URL = "https://ss21vv-externalweatherservice.azurewebsites.net/api/v1/authenticate";
    private static final String SERVICE_URL = "https://ss21vv-externalweatherservice.azurewebsites.net/api/WeatherForecast";

    private String user = EnvironmentVariables.VV_WEATHER_USER.getValue();
    private String password = EnvironmentVariables.VV_WEATHER_PASSWORD.getValue();

    private final Logger logger = LogManager.getLogger(WeatherForecastServiceImpl.class.getName());
    String accessToken;

    public ForecastData getWeatherData() throws DataFetchException, AuthenticationException {
        accessToken = createAccessToken();

        URI uri = null;
        try {
            uri = new URI(SERVICE_URL);
        } catch (URISyntaxException e) {
            throw new DataFetchException();
        }

        HttpRequest httpRequest = HttpRequest
                .newBuilder(uri)
                .setHeader("Authorization", "Bearer " + accessToken)
                .GET()
                .build();

        try {
            HttpResponse<String> httpResponse = sendRequest(httpRequest);
            if (httpResponse.statusCode() == 401) throw new AuthenticationException("401");
            else if (httpResponse.statusCode() != 200)
                throw new DataFetchException(String.valueOf(httpResponse.statusCode()));
            ForecastData forecastData = JSONDeserializer.deserializeObject(ForecastData.class, httpResponse.body());
            String msg = "Forecast-Data from: " + forecastData.getDate() + ", Value: " + forecastData.getSummary();
            logger.info(msg);
            return forecastData;
        } catch (IOException e) {
            throw new DataFetchException(e);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new DataFetchException(e);
        }
    }

    private String createAccessToken() throws AuthenticationException {
        if (accessToken != null) return accessToken;

        if (user == null || password == null) {
            throw new AuthenticationException("Username or Password not set in the environment variables!");
        }

        URI uri = null;
        try {
            uri = new URI(AUTH_URL);
        } catch (URISyntaxException e) {
            throw new AuthenticationException(e);
        }

        String body = "{\"username\":\"" + user + "\",\"password\":\"" + password + "\"}";

        HttpRequest httpRequest = HttpRequest
                .newBuilder(uri)
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .setHeader("Content-Type", "application/json")
                .build();

        try {
            HttpResponse<String> httpResponse = sendRequest(httpRequest);
            if (httpResponse.statusCode() != 200) {
                throw new AuthenticationException(String.valueOf(httpResponse.statusCode()));
            }
            String msg = "Access Granted! Your Token: " + httpResponse.body();
            logger.info(msg);
            return httpResponse.body();
        } catch (IOException e) {
            throw new AuthenticationException(e);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new AuthenticationException(e);
        }
    }

    private HttpResponse<String> sendRequest(HttpRequest request) throws IOException, InterruptedException {
        return HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
