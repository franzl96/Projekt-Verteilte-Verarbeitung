package infrastructure.proxyservice;

import application.interfaces.proxyservice.IHttpSmartHome;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
public class HttpSmartHomeServiceImpl implements IHttpSmartHome {

    private final Logger logger = LogManager.getLogger(HttpSmartHomeServiceImpl.class.getName());

    @Override
    public int sendNewStatusToActor(String serviceURL, String status) {

        URI uri = null;
        try {
            uri = new URI(serviceURL + "?status=" + status);
        } catch (URISyntaxException e) {
            logger.error("URI not valid");
            return 500;
        }

        String msg = uri.toString();
        logger.info(msg);

        try {
            HttpRequest httpRequest = HttpRequest.newBuilder(uri)
                    .setHeader("Content-Type", "application/json")
                    .POST(HttpRequest.BodyPublishers.noBody())
                    .build();
            return sendRequest(httpRequest);

        } catch (IllegalArgumentException e) {
            logger.error("URL scheme is invalid!! -> 500");
            return 500;
        }

    }

    private int sendRequest(HttpRequest httpRequest) {
        HttpClient httpClient = HttpClient.newHttpClient();
        try {
            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            return response.statusCode();
        } catch (IOException e) {
            if (e.getMessage() == null) return 500;
            if (e.getMessage().equals("Connection refused")) {
                return 503;
            }
            logger.error(e.getMessage());
            Thread.currentThread().interrupt();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        return 500;
    }
}
