package infrastructure.proxyservice;

import application.interfaces.proxyservice.IHttpSmartHome;
import application.interfaces.proxyservice.IProxy;
import application.interfaces.proxyservice.IWeatherForecast;
import domain.ForecastData;
import infrastructure.exceptions.AuthenticationException;
import infrastructure.exceptions.DataFetchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProxyImpl implements IProxy {

    private final IHttpSmartHome httpSmartHomeService;
    private final IWeatherForecast weatherForecastService;

    @Autowired
    public ProxyImpl(IHttpSmartHome httpSmartHomeService, IWeatherForecast weatherForecastService) {
        this.httpSmartHomeService = httpSmartHomeService;
        this.weatherForecastService = weatherForecastService;
    }


    @Override
    public int sendNewStatusToActor(String serviceURL, String status) {
        return httpSmartHomeService.sendNewStatusToActor(serviceURL, status);
    }

    @Override
    public ForecastData getWeatherData() throws DataFetchException, AuthenticationException {
        return weatherForecastService.getWeatherData();
    }
}

