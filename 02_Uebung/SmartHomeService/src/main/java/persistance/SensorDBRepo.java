package persistance;

import domain.Sensor;
import org.springframework.data.repository.CrudRepository;


public interface SensorDBRepo extends CrudRepository<Sensor, Long> {
}
