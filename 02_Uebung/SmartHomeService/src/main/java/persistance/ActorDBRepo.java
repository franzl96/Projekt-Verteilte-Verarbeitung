package persistance;

import domain.Actor;
import org.springframework.data.repository.CrudRepository;

public interface ActorDBRepo extends CrudRepository<Actor, Long> {


}
