package persistance;

import domain.SensorData;
import org.springframework.data.repository.CrudRepository;

public interface SensorDataDBRepo extends CrudRepository<SensorData, Long> {
}
