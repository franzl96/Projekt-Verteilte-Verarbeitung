package persistance;

import domain.Rule;
import org.springframework.data.repository.CrudRepository;

public interface RuleDBRepo extends CrudRepository<Rule, Long> {

    Rule findByRuleName(String name);

}
