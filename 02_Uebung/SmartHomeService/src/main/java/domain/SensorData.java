package domain;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.*;

@Entity
public class SensorData implements Identifiable {

    @Id
    @GeneratedValue
    @Schema(description = "Eine ID die Automatisch vom Server generiert wird.")
    private Long id;

    @Version
    private Integer version;

    @Schema(description = "Ein Zeitstempel der über den Sensor mitgegeben wird", example = "JJJJ-MM-JJTHH:SS:ss.ms", required = true)
    private String timestamp;

    @Schema(description = "Der aktuelle Wert den der Sensor gemessen hat", example = "20", required = true)
    private Integer currentValue;

    @Schema(description = "Die Einheit in der gemessen wurde ", example = "Celsius", required = true)
    private String unit;

    @ManyToOne
    private Sensor sensor;


    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        return this.toString().equals(obj.toString());
    }

    @Override
    public String toString() {
        return "SensorData{" +
                ", timestamp=" + timestamp +
                ", currentValue=" + currentValue +
                ", unit='" + unit + '\'' +
                '}';
    }

    //Getter and Setter
    public void setSensor(Sensor sensor) {
        this.sensor = sensor;
    }

    public String getSensorID() {
        return this.sensor.getID().toString();
    }

    @Override
    public Integer getVersion() {
        return version;
    }

    public Long getID() {
        return id;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }


    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(Integer currentValue) {
        this.currentValue = currentValue;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Override
    public int hashCode() {
        return super.hashCode();
    }
}

