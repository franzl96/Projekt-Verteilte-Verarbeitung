package domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;
import java.time.LocalDateTime;

@Entity
public class Actor implements Identifiable {

    @Id
    @Schema(description = "Eine eindeutige ID für den Aktor", example = "10", required = true)
    private Long id;

    @Version
    @Schema(description = "Ein Versionszähler für die Updates auf die Datenbank", example = "1", required = true)
    private Integer version;

    @Schema(description = "Der Name des Gerätes", example = "Markisenmotor", required = true)
    private String actorName;

    @Schema(description = "Das Hinzufügedatum des Gerätes -> Wird automatisch ermittelt")
    @Convert(converter = DateConverter.class)
    private LocalDateTime addedOn;

    @Schema(description = "Wo steht der Aktor", example = "Badezimmer", required = true)
    private String location;

    @Schema(description = "Was ist die Service URL des Aktors", example = "http://www.example.de", required = true)
    private String serviceURL;

    @Schema(description = "Gibt den Status des Actors aus")
    private String status;


    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        return this.toString().equals(obj.toString());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }


    @Override
    public String toString() {
        return "Actor{" +
                "actorID=" + id +
                ", actorName='" + actorName + '\'' +
                ", location=" + location +
                ", serviceURL=" + serviceURL +
                ", status='" + status + '\'' +
                '}';
    }

    //Getter and Setter

    @Override
    public Integer getVersion() {
        return version;
    }

    @JsonProperty(value = "AktorId")
    public Long getID() {
        return id;
    }

    public void setId(Long actorID) {
        this.id = actorID;
    }

    public String getActorName() {
        return actorName;
    }

    public void setActorName(String actorName) {
        this.actorName = actorName;
    }

    public LocalDateTime getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(LocalDateTime addedOn) {
        this.addedOn = addedOn;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getServiceURL() {
        return serviceURL;
    }

    public void setServiceURL(String serviceURL) {
        this.serviceURL = serviceURL;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
