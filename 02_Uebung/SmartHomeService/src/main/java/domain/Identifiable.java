package domain;

public interface Identifiable {
    Long getID();

    Integer getVersion();
}
