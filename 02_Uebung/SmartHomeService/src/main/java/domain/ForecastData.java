package domain;

import java.time.LocalDateTime;

public class ForecastData {

    private String date;
    private String summary;

    public ForecastData() {
    }

    public ForecastData(String date, String summary) {
        this.date = date;
        this.summary = summary;
    }

    public String getDate() {
        return date;
    }

    public LocalDateTime getDateAsLocalDate() {
        return LocalDateTime.parse(date);
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    @Override
    public String toString() {
        return "ForecastData{" +
                "date=" + date +
                ", summary='" + summary + '\'' +
                '}';
    }
}
