package domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.*;

@Entity
@Table(name = "Rule_Entity")
public class Rule implements Identifiable {

    @Id
    @GeneratedValue
    @Schema(description = "Eine ID die Automatisch vom Server generiert wird.")
    private Long id;

    @Version
    @Schema(description = "Ein Versionszähler für die Updates auf die Datenbank", example = "1", required = true)
    private Integer version;

    @Schema(description = "Ein einmaliger Name für die Regel", example = "Markise steuern")
    @Column(unique = true)
    private String ruleName;

    @Schema(description = "Eine ID für einen Sensor der im System hinterlegt ist", example = "1", required = true)
    private Long sensorID;
    @Schema(description = "Eine ID für einen Aktor der im System hinterlegt ist", example = "1", required = true)
    private Long actorID;

    @Schema(description = "Wert ab dem ein Aktor beim unter oder überschreiten eine Aktion ausführt",
            example = "1", required = true, minimum = "1", maximum = "29")
    private Integer threshold;


    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        return this.toString().equals(obj.toString());
    }

    @Override
    public String toString() {
        return "Rule{ruleName='" + ruleName + '\'' +
                ", sensorID=" + sensorID +
                ", actorID=" + actorID +
                ", threshold=" + threshold +
                '}';
    }

    //Getter and Setter
    @Override
    public Integer getVersion() {
        return version;
    }

    @JsonProperty(value = "RuleId")
    public Long getID() {
        return id;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public Long getSensorID() {
        return sensorID;
    }

    public void setSensorID(Long sensorID) {
        this.sensorID = sensorID;
    }

    public Long getActorID() {
        return actorID;
    }

    public void setActorID(Long actorID) {
        this.actorID = actorID;
    }

    public Integer getThreshold() {
        return threshold;
    }

    public void setThreshold(Integer threshold) {
        this.threshold = threshold;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
