package domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
public class Sensor implements Identifiable {

    @Id
    @Schema(description = "Eine eindeutige ID für den Sensor", example = "22", required = true)
    private Long id;

    @Version
    @Schema(description = "Ein Versionszähler für die Updates auf die Datenbank", example = "1", required = true)
    private Integer version;

    @Schema(description = "Der Name des Sensors", example = "Lichtsensor", required = true)
    private String sensorName;

    @Schema(description = "Das Hinzufügedatum des Gerätes -> Wird automatisch ermittelt")
    @Convert(converter = DateConverter.class)
    private LocalDateTime addedOn;

    @Schema(description = "Wo steht der Sensor", example = "Badezimmer", required = true)
    private String location;

    @Schema(description = "Liste der Sensordaten", example = "[]", required = false)
    @OneToMany(mappedBy = "sensor", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<SensorData> sensorDataList;


    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        return this.toString().equals(obj.toString());
    }

    @Override
    public String toString() {
        return "Sensor: " + getID() + ", " + getSensorName() + ", " + getLocation();
    }

    //Getter and Setter
    public List<SensorData> getSensorDataList() {
        return sensorDataList;
    }

    public void setSensorDataList(List<SensorData> sensorDataList) {
        this.sensorDataList = sensorDataList;
    }

    @Override
    public Integer getVersion() {
        return version;
    }

    @JsonProperty(value = "SensorId")
    public Long getID() {
        return id;
    }

    public void setId(Long sensorID) {
        this.id = sensorID;
    }

    public String getSensorName() {
        return sensorName;
    }

    public void setSensorName(String sensorName) {
        this.sensorName = sensorName;
    }

    public LocalDateTime getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(LocalDateTime addedOn) {
        this.addedOn = addedOn;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
