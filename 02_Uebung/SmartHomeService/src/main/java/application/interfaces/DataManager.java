package application.interfaces;

import application.common.exceptions.ContentAlreadyExistsException;
import application.common.exceptions.ContentNotAvailableException;
import application.common.exceptions.ContentNotDeletableException;
import application.common.exceptions.ContentVersionException;

import java.util.List;

public interface DataManager<T> {

    /**
     * Delivers a list of database entries
     *
     * @return of database entries
     */
    List<T> getListOfObjects();

    /**
     * Delivers a single database entry
     *
     * @param id the id of the entry
     * @return database entry depending on the id
     * @throws ContentNotAvailableException Is thrown if the entry is not available
     */
    T getSingleObject(Long id) throws ContentNotAvailableException;

    /**
     * Updates a single database entry
     *
     * @param id     the id of the entry
     * @param object the updated version of the entry
     * @return the updated version of the entry
     * @throws ContentNotAvailableException Is thrown if the entry is not available
     * @throws ContentVersionException      Is thrown if the version numbers are different
     */
    T updateSingleObject(Long id, T object) throws ContentNotAvailableException, ContentVersionException;

    /**
     * Deletes a single object from the database
     *
     * @param id the id of the entry
     * @return the deleted object
     * @throws ContentNotAvailableException Is thrown if the entry is not available
     * @throws ContentNotDeletableException This exception is thrown when anything depends of the other
     */
    T deleteObject(Long id) throws ContentNotAvailableException, ContentNotDeletableException;

    /**
     * Adding a single object
     *
     * @param content the object
     * @return the new created object!
     * @throws ContentAlreadyExistsException Is thrown when another object with the key exists!
     */
    T addSingleObject(T content) throws ContentAlreadyExistsException;


}