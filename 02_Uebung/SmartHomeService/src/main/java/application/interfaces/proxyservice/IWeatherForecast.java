package application.interfaces.proxyservice;

import domain.ForecastData;
import infrastructure.exceptions.AuthenticationException;
import infrastructure.exceptions.DataFetchException;

public interface IWeatherForecast {

    ForecastData getWeatherData() throws DataFetchException, AuthenticationException;

}
