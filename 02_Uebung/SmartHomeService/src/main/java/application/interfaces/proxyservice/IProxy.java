package application.interfaces.proxyservice;

import domain.ForecastData;
import infrastructure.exceptions.AuthenticationException;
import infrastructure.exceptions.DataFetchException;

/**
 * A service that makes the communication over http
 */
public interface IProxy {

    int sendNewStatusToActor(String serviceURL, String status);

    ForecastData getWeatherData() throws DataFetchException, AuthenticationException;

}
