package application.common.exceptions;

public class ContentAlreadyExistsException extends Exception {
    public ContentAlreadyExistsException() {
        super("The entry with this id or name already exists in the database");
    }
}


