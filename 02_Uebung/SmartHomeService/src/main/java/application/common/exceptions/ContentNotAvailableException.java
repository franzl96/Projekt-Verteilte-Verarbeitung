package application.common.exceptions;

public class ContentNotAvailableException extends Exception {
    public ContentNotAvailableException() {
        super("No entry for the sensor/actor id on server");
    }
}
