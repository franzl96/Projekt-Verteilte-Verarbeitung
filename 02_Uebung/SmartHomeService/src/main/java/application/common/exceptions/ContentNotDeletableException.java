package application.common.exceptions;

public class ContentNotDeletableException extends Exception {
    public ContentNotDeletableException() {
        super("The content isn't deletable because another database entry depends on this database entry");
    }
}
