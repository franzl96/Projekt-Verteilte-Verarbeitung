package application.common.exceptions;

public class ContentVersionException extends Exception {
    public ContentVersionException() {
        super("The version number is not the same as the number in the database (old data)");
    }
}
