package application.common;

public enum EnvironmentVariables {

    HOST_NAME("HostName"),
    VV_WEATHER_USER("VVWeatherUser"),
    VV_WEATHER_PASSWORD("VVWeatherPassword");

    private final String name;

    EnvironmentVariables(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getValue() {
        return System.getenv(name);
    }
}
