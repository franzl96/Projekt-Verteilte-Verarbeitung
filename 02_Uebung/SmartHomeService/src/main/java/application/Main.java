package application;

import infrastructure.LogicRuleEngine;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "persistance") // Import the persistence Package Repositorys
@EntityScan(basePackages = "domain") //Import the domain package entity's
@ComponentScan(basePackages = "presentation")
@ComponentScan(basePackages = "infrastructure")

public class Main {

    @Autowired
    private LogicRuleEngine logicRuleEngine;

    private static final Logger LOGGER = LogManager.getLogger(Main.class.getName());

    public static void main(String[] args) {
        LOGGER.info("Main is started!");
        SpringApplication.run(Main.class, args);
    }

    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI().components(new Components())
                .info(new Info()
                        .title("Smart Home Server")
                        .version("3")
                        .license(new License()
                                .name("MIT")
                                .url("http://springdoc.org")));
    }

    @Bean
    CommandLineRunner logicEngine() {
        return args -> logicRuleEngine.startLogicRuleEngine();
    }
}
