package application.datamanager;

import application.common.exceptions.ContentAlreadyExistsException;
import application.common.exceptions.ContentVersionException;
import domain.Rule;
import org.springframework.data.repository.CrudRepository;

public class RuleDataManager extends ADataManager<Rule> {
    /**
     * Initialize the manager via dependency injection
     *
     * @param dBRepo The crud repository of a entity
     */
    public RuleDataManager(CrudRepository<Rule, Long> dBRepo) {
        super(dBRepo);
    }

    @Override
    Rule updateDataFields(Rule updateValue, Rule dBValue) throws ContentVersionException {
        return null;
    }

    @Override
    Rule setDataFields(Rule dataValue) throws ContentAlreadyExistsException {
        if (checkIfItemAvailable(dataValue.getRuleName())) throw new ContentAlreadyExistsException();
        return dataValue;
    }
}
