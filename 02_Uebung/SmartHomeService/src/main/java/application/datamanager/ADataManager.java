package application.datamanager;

import application.common.exceptions.ContentAlreadyExistsException;
import application.common.exceptions.ContentNotAvailableException;
import application.common.exceptions.ContentNotDeletableException;
import application.common.exceptions.ContentVersionException;
import application.interfaces.DataManager;
import domain.Identifiable;
import domain.Rule;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.repository.CrudRepository;
import persistance.RuleDBRepo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class ADataManager<T> implements DataManager<T> {

    CrudRepository<T, Long> dBRepo;

    /**
     * Initialize the manager via dependency injection
     *
     * @param dBRepo The crud repository of a entity
     */
    protected ADataManager(CrudRepository<T, Long> dBRepo) {
        this.dBRepo = dBRepo;
    }

    @Override
    public List<T> getListOfObjects() {
        List<T> result = new ArrayList<>();
        dBRepo.findAll().forEach((result::add));
        return result;
    }

    @Override
    public T getSingleObject(Long id) throws ContentNotAvailableException {
        if (!checkIfItemAvailable(id)) throw new ContentNotAvailableException();
        Optional<T> result = dBRepo.findById(id);

        if (result.isEmpty()) throw new ContentNotAvailableException();
        return result.get();
    }

    @Override
    public T updateSingleObject(Long id, T object) throws ContentNotAvailableException, ContentVersionException {
        Identifiable identifiableUpdatet = (Identifiable) object;

        T sensorData = getSingleObject(id);
        Identifiable identifiableOld = (Identifiable) sensorData;

        if (identifiableUpdatet.getVersion() == null) throw new ContentVersionException();
        if (!identifiableUpdatet.getVersion().equals(identifiableOld.getVersion())) throw new ContentVersionException();
        T data = updateDataFields(object, sensorData);
        dBRepo.save(data);
        return getSingleObject(id);
    }

    @Override
    public T deleteObject(Long id) throws ContentNotAvailableException, ContentNotDeletableException {
        T object = getSingleObject(id);
        dBRepo.delete(object);
        return object;
    }

    @Override
    public T addSingleObject(T content) throws ContentAlreadyExistsException, DataIntegrityViolationException {
        return dBRepo.save(setDataFields(content));
    }


    public boolean checkIfItemAvailable(Long id) {
        return dBRepo.existsById(id);
    }

    public boolean checkIfItemAvailable(String name) {
        RuleDBRepo ruleDBRepo = (RuleDBRepo) dBRepo;
        if (ruleDBRepo == null) return false;
        Rule rule = ruleDBRepo.findByRuleName(name);
        return rule != null;
    }

    abstract T updateDataFields(T updateValue, T dBValue) throws ContentVersionException;

    abstract T setDataFields(T dataValue) throws ContentAlreadyExistsException;

}
