package application.datamanager;

import application.common.exceptions.ContentAlreadyExistsException;
import application.common.exceptions.ContentVersionException;
import domain.SensorData;
import org.springframework.data.repository.CrudRepository;

public class SensorDataDataManager extends ADataManager<SensorData> {
    /**
     * Initialize the manager via dependency injection
     *
     * @param dBRepo The crud repository of a entity
     */
    public SensorDataDataManager(CrudRepository<SensorData, Long> dBRepo) {
        super(dBRepo);
    }

    @Override
    SensorData updateDataFields(SensorData updateValue, SensorData dBValue) throws ContentVersionException {
        return null;
    }

    @Override
    SensorData setDataFields(SensorData dataValue) throws ContentAlreadyExistsException {
        return dataValue;
    }
}
