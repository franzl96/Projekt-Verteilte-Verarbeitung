package application.datamanager;

import application.common.exceptions.ContentAlreadyExistsException;
import domain.Sensor;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;


public class SensorDataManager extends ADataManager<Sensor> {

    /**
     * Initialize the manager via dependency injection
     *
     * @param dBRepo The crud repository of a entity
     */
    public SensorDataManager(CrudRepository<Sensor, Long> dBRepo) {
        super(dBRepo);
    }

    @Override
    Sensor updateDataFields(Sensor updateValue, Sensor dBValue) {
        if (updateValue.getSensorName() != null) {
            dBValue.setSensorName(updateValue.getSensorName());
        }
        if (updateValue.getLocation() != null) {
            dBValue.setLocation(updateValue.getLocation());
        }
        dBValue.setAddedOn(LocalDateTime.now());

        return dBValue;
    }

    @Override
    Sensor setDataFields(Sensor dataValue) throws ContentAlreadyExistsException {
        if (checkIfItemAvailable(dataValue.getID())) throw new ContentAlreadyExistsException();

        dataValue.setAddedOn(LocalDateTime.now());
        return dataValue;
    }
}
