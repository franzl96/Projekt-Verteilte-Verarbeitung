package application.datamanager;

import application.common.exceptions.ContentAlreadyExistsException;
import application.common.exceptions.ContentVersionException;
import domain.Actor;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;

public class ActorDataManager extends ADataManager<Actor> {
    /**
     * Initialize the manager via dependency injection
     *
     * @param dBRepo The crud repository of a entity
     */
    public ActorDataManager(CrudRepository<Actor, Long> dBRepo) {
        super(dBRepo);
    }

    @Override
    Actor updateDataFields(Actor updateValue, Actor dBValue) throws ContentVersionException {
        return updateValue;
    }

    @Override
    Actor setDataFields(Actor dataValue) throws ContentAlreadyExistsException {
        if (checkIfItemAvailable(dataValue.getID())) throw new ContentAlreadyExistsException();
        dataValue.setAddedOn(LocalDateTime.now());
        return dataValue;
    }
}
