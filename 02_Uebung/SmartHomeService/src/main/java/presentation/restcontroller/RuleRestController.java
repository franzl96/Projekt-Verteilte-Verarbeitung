package presentation.restcontroller;

import application.common.exceptions.ContentNotAvailableException;
import application.datamanager.ActorDataManager;
import application.datamanager.RuleDataManager;
import application.datamanager.SensorDataManager;
import application.interfaces.DataManager;
import domain.Actor;
import domain.Rule;
import domain.Sensor;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import persistance.ActorDBRepo;
import persistance.RuleDBRepo;
import persistance.SensorDBRepo;
import presentation.dtos.HttpErrorMessageDTO;
import presentation.dtos.RuleDTO;
import presentation.services.ModelMapperService;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

@RestController
public class RuleRestController {
    @Autowired
    private ModelMapperService modelMapperService;

    private final RuleDBRepo ruleDBRepo;
    private final SensorDBRepo sensorDBRepo;
    private final ActorDBRepo actorDBRepo;

    private StandardRestCommands<Rule> standardRestCommands;

    @Autowired
    public RuleRestController(RuleDBRepo ruleDBRepo, SensorDBRepo sensorDBRepo, ActorDBRepo actorDBRepo) {
        this.ruleDBRepo = ruleDBRepo;
        this.sensorDBRepo = sensorDBRepo;
        this.actorDBRepo = actorDBRepo;
    }

    @PostConstruct
    public void init() {
        DataManager<Rule> ruleDataManager = new RuleDataManager(ruleDBRepo);
        standardRestCommands = new StandardRestCommands<>(ruleDataManager);
    }

    @GetMapping(value = "api/v1/rules", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Abruf der aller im System registrierten Rules!")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Daten erfolgreich abgerufen!", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Rule.class))
            })
    })
    public ResponseEntity<Object> getAllRules() {
        return standardRestCommands.getAllEntries();
    }


    @GetMapping(value = "api/v1/rules/{value}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Abruf einer im System registrierten Regel!")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Daten erfolgreich abgerufen!", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Actor.class))
            }),
            @ApiResponse(responseCode = "404", description = "Die angegebene ID bzw. der Name ist nicht registriert!", content = {
                    @Content(mediaType = "application/json")
            })
    })
    public ResponseEntity<Object> getOneRuleWith(@PathVariable String value) {

        try {
            Long id = Long.parseLong(value);
            return standardRestCommands.getOneEntity(id);
        } catch (NumberFormatException e) {
            Rule rule = ruleDBRepo.findByRuleName(value);
            if (rule == null) return ResponseEntity.notFound().build();
            return ResponseEntity.ok(rule);
        }
    }


    @PostMapping(value = "api/v1/rules", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Anlegen einer Regel im System")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Daten erfolgreich angelegt!", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Actor.class))
            }),
            @ApiResponse(responseCode = "400", description = "Die Daten im Json Body sind nicht korrekt, oder eine Regel mit dem selben Namen existiert bereits.", content = {
                    @Content(mediaType = "application/json")
            }),
            @ApiResponse(responseCode = "404", description = "Die angegebene SensorID oder ActorID existiert nicht", content = {
                    @Content(mediaType = "application/json")
            })
    })
    public ResponseEntity<Object> addRule(@Valid @RequestBody RuleDTO ruleDTO) {

        Rule rule = modelMapperService.getModelMapper().map(ruleDTO, Rule.class);

        DataManager<Actor> actorDataManager = new ActorDataManager(actorDBRepo);
        DataManager<Sensor> sensorDataManager = new SensorDataManager(sensorDBRepo);

        try {
            actorDataManager.getSingleObject(rule.getActorID());
            sensorDataManager.getSingleObject(rule.getSensorID());
            return standardRestCommands.addSingleObject(rule, "rules");
        } catch (ContentNotAvailableException e) {
            return ResponseEntity.status(404).body(new HttpErrorMessageDTO(404, e.getMessage()));
        }
    }

    public void setModelMapperService(ModelMapperService modelMapperService) {
        this.modelMapperService = modelMapperService;
    }

}
