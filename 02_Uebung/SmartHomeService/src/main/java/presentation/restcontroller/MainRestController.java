package presentation.restcontroller;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainRestController {

    @GetMapping("")
    @Operation(description = "Show welcome-page")
    public ResponseEntity<String> welcomeOnServer() {
        String bodyMsg = "<h1>Willkommen auf dem Smart Home Server</h1>" +
                "Um mehr zur Implementierung zu erfahren rufen sie folgende Seite auf:" +
                "<a href = \"http://localhost:9000/swagger-ui.html\"> SwaggerUI </a>";
        return new ResponseEntity<>(bodyMsg, HttpStatus.OK);
    }


}
