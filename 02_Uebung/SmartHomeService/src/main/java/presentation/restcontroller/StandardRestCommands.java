package presentation.restcontroller;

import application.common.EnvironmentVariables;
import application.common.exceptions.ContentAlreadyExistsException;
import application.common.exceptions.ContentNotAvailableException;
import application.common.exceptions.ContentNotDeletableException;
import application.common.exceptions.ContentVersionException;
import application.interfaces.DataManager;
import domain.Identifiable;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import presentation.dtos.HttpErrorMessageDTO;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * In this class are standart commands there the same for all components.
 *
 * @param <T> This generic type describes the object that should be used
 */
public class StandardRestCommands<T> {

    public final String hostAddress = EnvironmentVariables.HOST_NAME.getValue() == null ? "localhost:9000" : EnvironmentVariables.HOST_NAME.getValue();

    private static final String PATH_DELIMITER = "/";

    private final DataManager<T> dataManager;

    public StandardRestCommands(DataManager<T> dataManager) {
        this.dataManager = dataManager;
    }

    /**
     * Gets all entries of an object
     *
     * @return A response entity with a JSON string depends on the result
     */
    public ResponseEntity<Object> getAllEntries() {
        return ResponseEntity.ok(dataManager.getListOfObjects());
    }

    /**
     * Gets a specific entry of an object
     *
     * @param id The id of the object
     * @return A response entity with a JSON string depends on the result
     */
    public ResponseEntity<Object> getOneEntity(Long id) {
        try {
            return ResponseEntity.ok(dataManager.getSingleObject(id));
        } catch (ContentNotAvailableException e) {
            return ResponseEntity.status(404).body(new HttpErrorMessageDTO(404, e.getMessage()));
        }
    }

    /**
     * Update a entry
     *
     * @param id     The id of the original object
     * @param object a new object that updates the original object
     * @return A response entity with a JSON string depends on the result
     */
    public ResponseEntity<Object> updateEntity(Long id, T object) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(dataManager.updateSingleObject(id, object));
        } catch (ContentNotAvailableException e) {
            return ResponseEntity.status(404).body(new HttpErrorMessageDTO(404, e.getMessage()));
        } catch (ContentVersionException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new HttpErrorMessageDTO(409, e.getMessage()));
        }

    }

    /**
     * Add a Object to the database
     *
     * @param object     The object that should be added
     * @param objectName The name for the path component
     * @return A JSON string with the result of the operation
     */
    public ResponseEntity<Object> addSingleObject(T object, String objectName) {
        try {
            T singleObject = dataManager.addSingleObject(object);
            Long objectId = ((Identifiable) singleObject).getID();
            return ResponseEntity.created(new URI("http://" + hostAddress + "/api/v1/" + objectName + PATH_DELIMITER + objectId)).body(singleObject);
        } catch (ContentAlreadyExistsException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new HttpErrorMessageDTO(400, e.getMessage()));
        } catch (DataIntegrityViolationException e) {
            return ResponseEntity.badRequest().body(new HttpErrorMessageDTO(400, e.getMessage()));
        } catch (URISyntaxException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new HttpErrorMessageDTO(500, e.getMessage()));
        }
    }

    /**
     * Add a Object to the database
     *
     * @param object The object that should be added
     * @return A JSON string with the result of the operation
     */
    public ResponseEntity<Object> addSingleObject(T object) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(dataManager.addSingleObject(object));
        } catch (ContentAlreadyExistsException e) {
            return ResponseEntity.badRequest().body(new HttpErrorMessageDTO(400, e.getMessage()));
        }
    }

    /**
     * Deletes a specific entry of an object
     *
     * @param id The id of the object
     * @return A JSON string with the result of the operation
     */
    public ResponseEntity<Object> deleteSingleObject(Long id) {
        try {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(dataManager.deleteObject(id));
        } catch (ContentNotAvailableException e) {
            return ResponseEntity.status(404).body(new HttpErrorMessageDTO(404, e.getMessage()));
        } catch (ContentNotDeletableException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new HttpErrorMessageDTO(400, e.getMessage()));
        }
    }
}
