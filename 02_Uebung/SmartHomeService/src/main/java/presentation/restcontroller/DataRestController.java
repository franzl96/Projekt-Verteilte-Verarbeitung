package presentation.restcontroller;

import application.common.exceptions.ContentNotAvailableException;
import application.datamanager.SensorDataDataManager;
import application.datamanager.SensorDataManager;
import application.interfaces.DataManager;
import domain.Sensor;
import domain.SensorData;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import persistance.SensorDBRepo;
import persistance.SensorDataDBRepo;
import presentation.dtos.SensorDataDTO;
import presentation.services.ModelMapperService;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

@RestController
public class DataRestController {

    @Autowired
    private ModelMapperService modelMapperService;

    private final SensorDBRepo sensorDBRepo;
    private final SensorDataDBRepo sensorDataDBRepo;

    private DataManager<Sensor> sensorDataManager;

    private StandardRestCommands<SensorData> sensorDataStandardRestCommands;

    @Autowired
    public DataRestController(SensorDataDBRepo sensorDataDBRepo, SensorDBRepo sensorDBRepo) {
        this.sensorDataDBRepo = sensorDataDBRepo;
        this.sensorDBRepo = sensorDBRepo;
    }

    @PostConstruct
    public void init() {
        this.sensorDataManager = new SensorDataManager(sensorDBRepo);
        DataManager<SensorData> sensorDataDataManager = new SensorDataDataManager(sensorDataDBRepo);
        sensorDataStandardRestCommands = new StandardRestCommands<>(sensorDataDataManager);
    }

    @PostMapping(value = "api/v1/sensors/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Anlegen eines Sensor Messdatensatzes im System")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Daten erfolgreich angelegt!", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = SensorData.class))
            }),
            @ApiResponse(responseCode = "400", description = "Die Daten im Json Body sind nicht korrekt!", content = {
                    @Content(mediaType = "application/json")
            }),
            @ApiResponse(responseCode = "404", description = "Die angegebene ID ist nicht registriert!", content = {
                    @Content(mediaType = "application/json")
            })
    })
    public ResponseEntity<Object> receiveSensorData(@PathVariable Long id, @Valid @RequestBody SensorDataDTO sensorDataDTO) {
        SensorData sensorData = modelMapperService.getModelMapper().map(sensorDataDTO, SensorData.class);

        Sensor sensor;
        try {
            sensor = sensorDataManager.getSingleObject(id);
        } catch (ContentNotAvailableException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        sensorData.setSensor(sensor);

        return sensorDataStandardRestCommands.addSingleObject(sensorData);
    }

    public void setModelMapperService(ModelMapperService modelMapperService) {
        this.modelMapperService = modelMapperService;
    }
}
