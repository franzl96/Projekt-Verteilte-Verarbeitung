package presentation.restcontroller;

import application.datamanager.ActorDataManager;
import application.interfaces.DataManager;
import domain.Actor;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import persistance.ActorDBRepo;
import presentation.dtos.ActorDTO;
import presentation.dtos.HttpErrorMessageDTO;
import presentation.services.ModelMapperService;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

@RestController
public class ActorRestController {
    @Autowired
    private ModelMapperService modelMapperService;

    private final ActorDBRepo actorDBRepo;
    private StandardRestCommands<Actor> standardRestCommands;

    @Autowired
    public ActorRestController(ActorDBRepo actorDBRepo) {
        this.actorDBRepo = actorDBRepo;
    }

    @PostConstruct
    public void init() {
        DataManager<Actor> actorDataManager = new ActorDataManager(actorDBRepo);
        standardRestCommands = new StandardRestCommands<>(actorDataManager);
    }

    @GetMapping(value = "api/v1/actors", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Abruf der aller im System registrierten Aktoren!")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Daten erfolgreich abgerufen!", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Actor.class))
            })
    })
    public ResponseEntity<Object> getAllActors() {
        return standardRestCommands.getAllEntries();
    }


    @GetMapping(value = "api/v1/actors/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Abruf eins im System registrierten Aktor!")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Daten erfolgreich abgerufen!", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Actor.class))
            }),
            @ApiResponse(responseCode = "404", description = "Die angegebene ID ist nicht registriert!", content = {
                    @Content(mediaType = "application/json")
            })
    })
    public ResponseEntity<Object> getOneActor(@PathVariable Long id) {
        return standardRestCommands.getOneEntity(id);
    }


    @PostMapping(value = "api/v1/actors", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Registrieren eines Aktor im System")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Daten erfolgreich angelegt!", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Actor.class))
            }),
            @ApiResponse(responseCode = "400", description = "Die Daten im Json Body sind nicht korrekt, oder der Actor mit der ID existiert schon", content = {
                    @Content(mediaType = "application/json")
            }),
            @ApiResponse(responseCode = "500", description = "Es gab ein problem mit der Service-URL", content = {
                    @Content(mediaType = "application/json")
            })
    })
    public ResponseEntity<Object> addActor(@Valid @RequestBody ActorDTO actorDTO, HttpServletRequest request) {

        //This lines are for the case if the service-url not localhost (Docker-Compose name)
        try {
            if (request != null) {
                URI uri = new URI(actorDTO.getServiceURL());
                String generatedServiceURL = uri.getScheme() + "://" + request.getRemoteAddr() + ":" + uri.getPort() + uri.getPath();

                if (uri.getHost().equals("localhost")) { //Bad solution but it works (workaround of the bad dozent actor design)
                    generatedServiceURL = uri.getScheme() + "://" + request.getRemoteAddr() + ":" + "80" + uri.getPath();
                }
                actorDTO.setServiceURL(generatedServiceURL);
            }
            //--------------------

            Actor actor = this.modelMapperService.getModelMapper().map(actorDTO, Actor.class);
            return standardRestCommands.addSingleObject(actor, "actors");
        } catch (URISyntaxException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new HttpErrorMessageDTO(500, e.getMessage()));
        }
    }

    public void setModelMapperService(ModelMapperService modelMapperService) {
        this.modelMapperService = modelMapperService;
    }
}
