package presentation.restcontroller;

import application.common.exceptions.ContentNotDeletableException;
import application.datamanager.RuleDataManager;
import application.datamanager.SensorDataManager;
import application.interfaces.DataManager;
import domain.Actor;
import domain.Rule;
import domain.Sensor;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import persistance.RuleDBRepo;
import persistance.SensorDBRepo;
import presentation.dtos.HttpErrorMessageDTO;
import presentation.dtos.SensorDTO;
import presentation.services.ModelMapperService;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.util.List;

@RestController
public class SensorRestController {
    @Autowired
    private ModelMapperService modelMapperService;

    private final SensorDBRepo sensorDBRepo;
    private final RuleDBRepo ruleDBRepo;
    private StandardRestCommands<Sensor> standardRestCommands;

    public SensorRestController(SensorDBRepo sensorDBRepo, RuleDBRepo ruleDBRepo) {
        this.sensorDBRepo = sensorDBRepo;
        this.ruleDBRepo = ruleDBRepo;
    }

    @PostConstruct
    public void init() {
        DataManager<Sensor> sensorDataManager = new SensorDataManager(sensorDBRepo);
        standardRestCommands = new StandardRestCommands<>(sensorDataManager);
    }

    @GetMapping(value = "api/v1/sensors", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Abruf aller im System registrierten Sensoren!")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Daten erfolgreich abgerufen!", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Actor.class))
            })
    })
    public ResponseEntity<Object> getAllSensors() {
        return standardRestCommands.getAllEntries();
    }

    @GetMapping(value = "api/v1/sensors/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Abruf eins im System registrierten Sensors!")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Daten erfolgreich abgerufen!", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Actor.class))
            }),
            @ApiResponse(responseCode = "404", description = "Die angegebene ID ist nicht registriert!", content = {
                    @Content(mediaType = "application/json")
            })
    })
    public ResponseEntity<Object> getOneSensor(@PathVariable Long id) {
        return standardRestCommands.getOneEntity(id);
    }

    @PutMapping(value = "api/v1/sensors/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Updaten eines Sensors im System")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Daten erfolgreich angelegt!", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Actor.class))
            }),
            @ApiResponse(responseCode = "400", description = "Die Daten im Json Body sind nicht korrekt", content = {
                    @Content(mediaType = "application/json")
            }),
            @ApiResponse(responseCode = "404", description = "Die angegebene ID ist nicht registriert!", content = {
                    @Content(mediaType = "application/json")
            }),
            @ApiResponse(responseCode = "409", description = "Es gab einen Konflikt mit der Datenbank Version", content = {
                    @Content(mediaType = "application/json")
            })

    })
    public ResponseEntity<Object> updateSensor(@PathVariable Long id, @Valid @RequestBody SensorDTO sensorDTO) {
        Sensor sensor = modelMapperService.getModelMapper().map(sensorDTO, Sensor.class);
        return standardRestCommands.updateEntity(id, sensor);
    }

    @PostMapping(value = "api/v1/sensors", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Registrieren eines Sensors im System")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Daten erfolgreich angelegt!", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Actor.class))
            }),
            @ApiResponse(responseCode = "400", description = "Die Daten im Json Body sind nicht korrekt, oder der Sensor mit der ID existiert schon", content = {
                    @Content(mediaType = "application/json")
            })
    })
    public ResponseEntity<Object> addSensor(@Valid @RequestBody SensorDTO sensorDTO) {
        Sensor sensor = modelMapperService.getModelMapper().map(sensorDTO, Sensor.class);
        return standardRestCommands.addSingleObject(sensor, "sensors");
    }

    @DeleteMapping(value = "api/v1/sensors/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Löschen eines Sensors aus dem System, wenn der Sensor Sensordaten zu hatte, werden diese mitgelöscht!")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Daten erfolgreich gelöscht!", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Actor.class))
            }),
            @ApiResponse(responseCode = "400", description = "Die Daten können nicht gelöscht werden, da eine Abhängigkeit zu einer Regel besteht.", content = {
                    @Content(mediaType = "application/json")
            }),
            @ApiResponse(responseCode = "404", description = "Die angegebene ID ist nicht registriert!", content = {
                    @Content(mediaType = "application/json")
            }),
    })
    public ResponseEntity<Object> deleteSensor(@PathVariable Long id) {
        try {
            RuleDataManager ruleDataManager = new RuleDataManager(ruleDBRepo);
            List<Rule> ruleEntities = ruleDataManager.getListOfObjects();
            for (Rule rule : ruleEntities) {
                if (rule.getSensorID().equals(id)) {
                    throw new ContentNotDeletableException();
                }
            }

            return standardRestCommands.deleteSingleObject(id);
        } catch (ContentNotDeletableException e) {
            return ResponseEntity.
                    status(HttpStatus.BAD_REQUEST).
                    body(new HttpErrorMessageDTO(400, e.getMessage()));
        }
    }

    public void setModelMapperService(ModelMapperService modelMapperService) {
        this.modelMapperService = modelMapperService;
    }
}
