package presentation.dtos;

import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

public class HttpErrorMessageDTO {
    private final String timestamp;
    private final Integer status;
    private String error = "";
    private final String message;

    public HttpErrorMessageDTO(Integer status, String message) {
        this.timestamp = LocalDateTime.now().toString();
        this.status = status;
        HttpStatus httpStatus = HttpStatus.resolve(status);
        if (httpStatus != null) {
            this.error = httpStatus.toString();
        }
        this.message = message;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public Integer getStatus() {
        return status;
    }

    public String getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }
}
