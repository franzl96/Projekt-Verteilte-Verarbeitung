package presentation.dtos;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

public class SensorDataDTO {

    private Integer version;
    @NotNull
    private String timestamp;
    @NotNull
    @Range(min = 0, max = 30)
    private Integer currentValue;
    @NotNull
    private String unit;

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getTimestamp() {
        return timestamp;
    }

    @JsonProperty(value = "timestamp")
    @JsonAlias(value = "GeneratedAt")
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getCurrentValue() {
        return currentValue;
    }

    @JsonProperty(value = "currentValue")
    @JsonAlias(value = "Temperature")
    public void setCurrentValue(Integer currentValue) {
        this.currentValue = currentValue;
    }

    public String getUnit() {
        return unit;
    }

    @JsonProperty(value = "unit")
    @JsonAlias(value = "TemperatureUnit")
    public void setUnit(String unit) {
        this.unit = unit;
    }
}

