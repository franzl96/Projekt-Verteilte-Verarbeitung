package presentation.dtos;

import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

public class RuleDTO {

    private Integer version;
    @NotNull
    private String ruleName;
    @NotNull
    private Long sensorID;
    @NotNull
    private Long actorID;
    @NotNull
    @Range(min = 1, max = 29)
    private Integer threshold;


    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public Long getSensorID() {
        return sensorID;
    }

    public void setSensorID(Long sensorID) {
        this.sensorID = sensorID;
    }

    public Long getActorID() {
        return actorID;
    }

    public void setActorID(Long actorID) {
        this.actorID = actorID;
    }

    public Integer getThreshold() {
        return threshold;
    }

    public void setThreshold(Integer threshold) {
        this.threshold = threshold;
    }
}
