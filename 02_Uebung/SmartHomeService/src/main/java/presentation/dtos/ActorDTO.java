package presentation.dtos;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class ActorDTO {

    @NotNull
    @Min(1)
    private Long id;
    private Integer version;
    @NotNull
    private String actorName;
    @NotNull
    private String location;
    @NotNull
    @Pattern(regexp = "http[s]{0,1}:\\/\\/(www.){0,1}([a-zA-Z0-9])+(\\.[a-z]{2,}){0,1}(:[0-9]+){0,1}(\\/([a-zA-Z0-9])+){0,100}")
    //Limit because Recursion
    private String serviceURL;

    public Long getId() {
        return id;
    }

    @JsonProperty(value = "AktorId")
    @JsonAlias(value = "id")
    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getActorName() {
        return actorName;
    }

    @JsonProperty(value = "AktorName")
    @JsonAlias(value = "actorName")
    public void setActorName(String actorName) {
        this.actorName = actorName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getServiceURL() {
        return serviceURL;
    }

    public void setServiceURL(String serviceURL) {
        this.serviceURL = serviceURL;
    }
}

