package presentation.restcontroller;

import domain.Actor;
import domain.Rule;
import domain.Sensor;
import domain.SensorData;
import presentation.dtos.ActorDTO;
import presentation.dtos.RuleDTO;
import presentation.dtos.SensorDTO;
import presentation.dtos.SensorDataDTO;

import java.time.LocalDateTime;

public class DummyObjectGenerator {
    static Actor createDummyActor(Long id, Integer version) {
        Actor actor = new Actor();
        actor.setId(id);
        actor.setVersion(version);
        actor.setActorName("Test" + id);
        actor.setLocation("Testzimmer" + id);
        actor.setAddedOn(LocalDateTime.of(2020, 1, 21, 12, 00));
        actor.setServiceURL("http://localhost:8080/");

        return actor;
    }

    static ActorDTO createDummyActorDTO(Long id, Integer version) {
        ActorDTO actor = new ActorDTO();
        actor.setId(id);
        actor.setVersion(version);
        actor.setActorName("Test" + id);
        actor.setLocation("Testzimmer" + id);
        actor.setServiceURL("http://localhost:8080/");

        return actor;
    }


    static Sensor createDummySensor(Long id, int version) {
        Sensor sensor = new Sensor();
        sensor.setId(id);
        sensor.setSensorName("Test" + id);
        sensor.setLocation("Testzimmer" + id);
        sensor.setVersion(version);
        sensor.setAddedOn(LocalDateTime.of(2020, 1, 21, 12, 00));

        return sensor;
    }

    static SensorDTO createDummySensorDTO(Long id, Integer version) {
        SensorDTO sensor = new SensorDTO();
        sensor.setId(id);
        sensor.setSensorName("Test" + id);
        sensor.setLocation("Testzimmer" + id);
        sensor.setVersion(version);

        return sensor;
    }

    static Rule createDummyRule(Long sensorID, Long actorID, String name, Integer version) {
        Rule rule = new Rule();
        rule.setThreshold(20);
        rule.setSensorID(sensorID);
        rule.setActorID(actorID);
        rule.setVersion(version);
        rule.setRuleName(name);
        return rule;
    }

    static RuleDTO createDummyRuleDTO(Long sensorID, Long actorID, String name, Integer version) {
        RuleDTO rule = new RuleDTO();
        rule.setThreshold(20);
        rule.setSensorID(sensorID);
        rule.setActorID(actorID);
        rule.setVersion(version);
        rule.setRuleName(name);
        return rule;
    }

    static SensorDataDTO createSensorDataDTO() {
        SensorDataDTO sensorDataDTO = new SensorDataDTO();
        sensorDataDTO.setVersion(0);
        sensorDataDTO.setCurrentValue(20);
        sensorDataDTO.setUnit("Celsius");
        sensorDataDTO.setTimestamp(LocalDateTime.MAX.toString());

        return sensorDataDTO;
    }

    static SensorData createSensorData() {
        SensorData sensorData = new SensorData();
        sensorData.setSensor(createDummySensor(1L, 0));
        sensorData.setVersion(1);
        sensorData.setCurrentValue(20);
        sensorData.setUnit("Celsius");
        sensorData.setTimestamp(LocalDateTime.MAX.toString());
        return sensorData;
    }
}
