package presentation.restcontroller;

import domain.Actor;
import domain.Sensor;
import domain.SensorData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import persistance.SensorDBRepo;
import persistance.SensorDataDBRepo;
import presentation.dtos.ActorDTO;
import presentation.dtos.SensorDTO;
import presentation.dtos.SensorDataDTO;
import presentation.services.ModelMapperService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.*;

class DataRestControllerTest {
    private DataRestController controller;
    private SensorDBRepo sensorDBRepo;
    private SensorDataDBRepo sensorDataDBRepo;

    private final Sensor dummySensor = DummyObjectGenerator.createDummySensor(1L, 1);
    private final SensorDTO dummySensorDTO = DummyObjectGenerator.createDummySensorDTO(1L, 1);

    private final SensorData dummySensorData = DummyObjectGenerator.createSensorData();
    private final SensorDataDTO dummySensorDataDTO = DummyObjectGenerator.createSensorDataDTO();


    @BeforeEach
    void initialize() {
        sensorDBRepo = mock(SensorDBRepo.class);
        sensorDataDBRepo = mock(SensorDataDBRepo.class);
        controller = new DataRestController(sensorDataDBRepo, sensorDBRepo);
        controller.setModelMapperService(new ModelMapperService());
        controller.init();
    }

    @Test
    void receiveSensorDataTestSuccess() {
        when(sensorDBRepo.existsById(1L)).thenReturn(true);
        when(sensorDBRepo.findById(1L)).thenReturn(java.util.Optional.of(dummySensor));
        when(sensorDataDBRepo.save(dummySensorData)).thenReturn(dummySensorData);

        ResponseEntity<Object> responseEntity = controller.receiveSensorData(1L, dummySensorDataDTO);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(dummySensorData, responseEntity.getBody());

        verify(sensorDBRepo, times(1)).existsById(1L);
        verify(sensorDBRepo, times(1)).findById(1L);
        verify(sensorDataDBRepo, times(1)).save(dummySensorData);
    }

    @Test
    void receiveSensorDataTestFail() {
        when(sensorDBRepo.existsById(1L)).thenReturn(false);
        when(sensorDataDBRepo.save(dummySensorData)).thenReturn(dummySensorData);

        ResponseEntity<Object> responseEntity = controller.receiveSensorData(1L, dummySensorDataDTO);

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        assertNotEquals(dummySensorData, responseEntity.getBody());

        verify(sensorDBRepo, times(1)).existsById(1L);
        verify(sensorDataDBRepo, times(0)).save(dummySensorData);
    }
}