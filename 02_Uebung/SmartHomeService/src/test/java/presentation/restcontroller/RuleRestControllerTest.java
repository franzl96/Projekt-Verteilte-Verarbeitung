package presentation.restcontroller;

import domain.Actor;
import domain.Rule;
import domain.Sensor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import persistance.ActorDBRepo;
import persistance.RuleDBRepo;
import persistance.SensorDBRepo;
import presentation.dtos.ActorDTO;
import presentation.dtos.RuleDTO;
import presentation.dtos.SensorDTO;
import presentation.services.ModelMapperService;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class RuleRestControllerTest {

    RuleDBRepo ruleDBRepo;
    ActorDBRepo actorDBRepo;
    SensorDBRepo sensorDBRepo;

    RuleRestController controller;

    private final Actor dummyActor = DummyObjectGenerator.createDummyActor(1L, 0);
    private final ActorDTO dummyActorDTO = DummyObjectGenerator.createDummyActorDTO(1L, 0);

    private final Sensor dummySensor = DummyObjectGenerator.createDummySensor(1L, 1);
    private final SensorDTO dummySensorDTO = DummyObjectGenerator.createDummySensorDTO(1L, 1);

    private final Rule dummyRule = DummyObjectGenerator.createDummyRule(1L, 1L, "Testrule", 1);
    private final RuleDTO dummyRuleDTO = DummyObjectGenerator.createDummyRuleDTO(1L, 1L, "Testrule", 1);

    @BeforeEach
    void initialize() {
        ruleDBRepo = mock(RuleDBRepo.class);
        actorDBRepo = mock(ActorDBRepo.class);
        sensorDBRepo = mock(SensorDBRepo.class);

        controller = new RuleRestController(ruleDBRepo, sensorDBRepo, actorDBRepo);
        controller.setModelMapperService(new ModelMapperService());
        controller.init();
    }

    @Test
    void getOneRulePerNameWithSuccess() {
        when(ruleDBRepo.findByRuleName("Testrule")).thenReturn(dummyRule);

        ResponseEntity<Object> responseEntity = controller.getOneRuleWith("Testrule");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(dummyRule, responseEntity.getBody());

        verify(ruleDBRepo, times(1)).findByRuleName("Testrule");
    }

    @Test
    void getOneRulePerNameWithError() {
        when(ruleDBRepo.findByRuleName("Testrule")).thenReturn(dummyRule);

        ResponseEntity<Object> responseEntity = controller.getOneRuleWith("Teerule");
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());

        verify(ruleDBRepo, times(0)).findByRuleName("Testrule");
    }

    @Test
    void getOneRulePerIDWithSuccess() {
        when(ruleDBRepo.findById(1L)).thenReturn(Optional.of(dummyRule));
        when(ruleDBRepo.existsById(1L)).thenReturn(true);

        ResponseEntity<Object> responseEntity = controller.getOneRuleWith("1");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(dummyRule, responseEntity.getBody());

        verify(ruleDBRepo, times(1)).findById(1L);
        verify(ruleDBRepo, times(1)).existsById(1L);
    }

    @Test
    void getOneRulePerIDWithError() {
        when(ruleDBRepo.findById(1L)).thenReturn(Optional.of(dummyRule));
        when(ruleDBRepo.existsById(1L)).thenReturn(true);

        ResponseEntity<Object> responseEntity = controller.getOneRuleWith("7");
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());

        verify(ruleDBRepo, times(0)).findById(1L);
        verify(ruleDBRepo, times(0)).existsById(1L);
    }

    @Test
    void addRuleTestNoRuleExist() {
        when(actorDBRepo.existsById(1L)).thenReturn(true);
        when(actorDBRepo.findById(1L)).thenReturn(Optional.of(dummyActor));
        when(sensorDBRepo.existsById(1L)).thenReturn(true);
        when(sensorDBRepo.findById(1L)).thenReturn(Optional.of(dummySensor));
        when(ruleDBRepo.save(dummyRule)).thenReturn(dummyRule);

        ResponseEntity<Object> responseEntity = controller.addRule(dummyRuleDTO);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(dummyRule, responseEntity.getBody());

        verify(actorDBRepo, times(1)).existsById(1L);
        verify(actorDBRepo, times(1)).findById(1L);
        verify(sensorDBRepo, times(1)).existsById(1L);
        verify(sensorDBRepo, times(1)).findById(1L);
        verify(ruleDBRepo, times(1)).save(dummyRule);
    }

    @Test
    void addRuleTestRuleExist() {
        when(actorDBRepo.existsById(1L)).thenReturn(true);
        when(actorDBRepo.findById(1L)).thenReturn(Optional.of(dummyActor));
        when(sensorDBRepo.existsById(1L)).thenReturn(true);
        when(sensorDBRepo.findById(1L)).thenReturn(Optional.of(dummySensor));
        when(ruleDBRepo.findByRuleName("Testrule")).thenReturn(dummyRule);
        when(ruleDBRepo.save(dummyRule)).thenReturn(dummyRule);

        ResponseEntity<Object> responseEntity = controller.addRule(dummyRuleDTO);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());

        verify(actorDBRepo, times(1)).existsById(1L);
        verify(actorDBRepo, times(1)).findById(1L);
        verify(sensorDBRepo, times(1)).existsById(1L);
        verify(sensorDBRepo, times(1)).findById(1L);
        verify(ruleDBRepo, times(0)).save(dummyRule);
    }
}