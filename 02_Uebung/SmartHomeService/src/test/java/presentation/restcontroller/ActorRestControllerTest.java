package presentation.restcontroller;

import domain.Actor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import persistance.ActorDBRepo;
import presentation.dtos.ActorDTO;
import presentation.services.ModelMapperService;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class ActorRestControllerTest {


    private ActorRestController controller;
    private ActorDBRepo repo;

    private final Actor dummyActor = DummyObjectGenerator.createDummyActor(1L, 0);
    private final ActorDTO dummyActorDTO = DummyObjectGenerator.createDummyActorDTO(1L, 0);

    @BeforeEach
    void initialize() {
        repo = mock(ActorDBRepo.class);
        controller = new ActorRestController(repo);
        controller.setModelMapperService(new ModelMapperService());
        controller.init();
    }

    @Test
    void getAllActorsTest() {
        when(repo.findAll()).thenReturn(new ArrayList<>());

        ResponseEntity<Object> stringResponseEntity = controller.getAllActors();
        assertEquals(HttpStatus.OK, stringResponseEntity.getStatusCode());

        verify(repo, times(1)).findAll();
    }

    @Test
    void getOneActorTestEmpty() {
        when(repo.findById(1L)).thenReturn(null);
        ResponseEntity<Object> responseEntity = controller.getOneActor(1L);
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        verify(repo, times(0)).findById(1L);

    }

    @Test
    void getOneActorTestOneEntry() {
        when(repo.findById(1L)).thenReturn(java.util.Optional.of(dummyActor));
        when(repo.existsById(1L)).thenReturn(true);
        ResponseEntity<Object> responseEntity = controller.getOneActor(1L);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(dummyActor, responseEntity.getBody());

        verify(repo, times(1)).findById(1L);
        verify(repo, times(1)).existsById(1L);
    }


    @Test
    void addActorTestNoExistingActor() {

        when(repo.existsById(1L)).thenReturn(false);
        when(repo.save(dummyActor)).thenReturn(dummyActor);

        ResponseEntity<Object> responseEntity = controller.addActor(dummyActorDTO, null);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(dummyActor, responseEntity.getBody());

        verify(repo, times(1)).existsById(1L);
        verify(repo, times(1)).save(dummyActor);

    }

    @Test
    void addActorTestActorAlreadyExists() {
        when(repo.existsById(1L)).thenReturn(true);

        ResponseEntity<Object> responseEntity = controller.addActor(DummyObjectGenerator.createDummyActorDTO(1L, 0), null);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        verify(repo, times(1)).existsById(1L);
    }
}