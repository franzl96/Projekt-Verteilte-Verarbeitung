package presentation.restcontroller;

import domain.Rule;
import domain.Sensor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import persistance.RuleDBRepo;
import persistance.SensorDBRepo;
import presentation.dtos.SensorDTO;
import presentation.services.ModelMapperService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class SensorRestControllerTest {
    private SensorRestController controller;
    private SensorDBRepo sensorDBRepo;
    private RuleDBRepo ruleDBRepo;

    private final Sensor dummySensor1V1 = DummyObjectGenerator.createDummySensor(1L, 1);
    private final Sensor dummySensor2V1 = DummyObjectGenerator.createDummySensor(2L, 1);
    private final Sensor dummySensor2V2 = DummyObjectGenerator.createDummySensor(2L, 2);

    private final SensorDTO dummySensorDTO1V1 = DummyObjectGenerator.createDummySensorDTO(1L, 1);
    private final SensorDTO dummySensorDTO2V1 = DummyObjectGenerator.createDummySensorDTO(2L, 1);
    private final SensorDTO dummySensorDTO2V2 = DummyObjectGenerator.createDummySensorDTO(2L, 2);

    private final Rule dummyRule = DummyObjectGenerator.createDummyRule(1L, 1L, "Testrule", 1);


    @BeforeEach
    void initialize() {
        sensorDBRepo = mock(SensorDBRepo.class);
        ruleDBRepo = mock(RuleDBRepo.class);
        controller = new SensorRestController(sensorDBRepo, ruleDBRepo);
        controller.setModelMapperService(new ModelMapperService());
        controller.init();
    }

    @Test
    void getAllSensorsTest() {
        when(sensorDBRepo.findAll()).thenReturn(new ArrayList<>());

        ResponseEntity<Object> stringResponseEntity = controller.getAllSensors();
        assertEquals(HttpStatus.OK, stringResponseEntity.getStatusCode());

        verify(sensorDBRepo, times(1)).findAll();
    }

    @Test
    void getOneSensorTestEmpty() {
        when(sensorDBRepo.findById(1L)).thenReturn(null);
        ResponseEntity<Object> responseEntity = controller.getOneSensor(1L);
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());

        verify(sensorDBRepo, times(0)).findAll();
    }

    @Test
    void getOneSensorTestOneEntry() {
        when(sensorDBRepo.findById(1L)).thenReturn(java.util.Optional.of(dummySensor1V1));
        when(sensorDBRepo.existsById(1L)).thenReturn(true);
        ResponseEntity<Object> responseEntity = controller.getOneSensor(1L);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(dummySensor1V1, responseEntity.getBody());

        verify(sensorDBRepo, times(1)).findById(1L);
        verify(sensorDBRepo, times(1)).existsById(1L);
    }

    @Test
    void addSensorNoExistingSensor() {
        when(sensorDBRepo.existsById(1L)).thenReturn(false);
        when(sensorDBRepo.save(dummySensor1V1)).thenReturn(dummySensor1V1);

        ResponseEntity<Object> responseEntity = controller.addSensor(dummySensorDTO1V1);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(dummySensor1V1, responseEntity.getBody());

        verify(sensorDBRepo, times(1)).existsById(1L);
        verify(sensorDBRepo, times(1)).save(dummySensor1V1);
    }

    @Test
    void addSensorTestSensorAlreadyExists() {
        when(sensorDBRepo.existsById(1L)).thenReturn(true);

        ResponseEntity<Object> responseEntity = controller.addSensor(dummySensorDTO1V1);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());

        verify(sensorDBRepo, times(1)).existsById(1L);
    }

    @Test
    void updateSensorTestSuccess() {
        when(sensorDBRepo.existsById(1L)).thenReturn(true);
        when(sensorDBRepo.findById(1L)).thenReturn(java.util.Optional.of(dummySensor1V1));
        when(sensorDBRepo.save(dummySensor2V1)).thenReturn(dummySensor2V2);

        ResponseEntity<Object> responseEntity = controller.updateSensor(1L, dummySensorDTO2V1);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());

        verify(sensorDBRepo, times(2)).existsById(1L);
        verify(sensorDBRepo, times(2)).findById(1L);
        verify(sensorDBRepo, times(1)).save(any(Sensor.class));
    }

    @Test
    void updateSensorTestFail() {
        when(sensorDBRepo.existsById(1L)).thenReturn(true);
        when(sensorDBRepo.findById(1L)).thenReturn(java.util.Optional.of(dummySensor1V1));
        when(sensorDBRepo.save(dummySensor2V2)).thenReturn(dummySensor1V1);

        ResponseEntity<Object> responseEntity = controller.updateSensor(1L, dummySensorDTO2V2);
        assertEquals(HttpStatus.CONFLICT, responseEntity.getStatusCode());

        verify(sensorDBRepo, times(1)).existsById(1L);
        verify(sensorDBRepo, times(1)).findById(1L);
        verify(sensorDBRepo, times(0)).save(any(Sensor.class));
    }

    @Test
    void deleteSensorTestRuleExisting() {
        List<Rule> list = new ArrayList<>();
        list.add(dummyRule);
        when(ruleDBRepo.findAll()).thenReturn(list);
        when(sensorDBRepo.existsById(1L)).thenReturn(true);
        when(sensorDBRepo.findById(1L)).thenReturn(java.util.Optional.of(dummySensor1V1));

        ResponseEntity<Object> responseEntity = controller.deleteSensor(1L);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());

        verify(ruleDBRepo, times(1)).findAll();
        verify(sensorDBRepo, times(0)).existsById(1L);
        verify(sensorDBRepo, times(0)).findById(1L);
    }

    @Test
    void deleteSensorTestRuleNotExisting() {
        List<Rule> list = new ArrayList<>();
        when(ruleDBRepo.findAll()).thenReturn(list);
        when(sensorDBRepo.existsById(1L)).thenReturn(true);
        when(sensorDBRepo.findById(1L)).thenReturn(java.util.Optional.of(dummySensor1V1));

        ResponseEntity<Object> responseEntity = controller.deleteSensor(1L);

        assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
        assertEquals(dummySensor1V1, responseEntity.getBody());

        verify(ruleDBRepo, times(1)).findAll();
        verify(sensorDBRepo, times(1)).existsById(1L);
        verify(sensorDBRepo, times(1)).findById(1L);
    }

    @Test
    void deleteNotExistingSensor() {
        when(sensorDBRepo.existsById(1L)).thenReturn(false);
        when(sensorDBRepo.findById(1L)).thenReturn(java.util.Optional.of(dummySensor1V1));

        ResponseEntity<Object> responseEntity = controller.deleteSensor(1L);
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());

        verify(sensorDBRepo, times(1)).existsById(1L);
        verify(sensorDBRepo, times(0)).findById(1L);
    }
}