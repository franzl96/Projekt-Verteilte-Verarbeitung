package infrastructure.proxyservice;

import infrastructure.exceptions.AuthenticationException;
import infrastructure.exceptions.DataFetchException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeatherForecastServiceImplTest {


    private WeatherForecastServiceImpl weatherForecastService = new WeatherForecastServiceImpl();


    @Test
    void getWeatherDataTest() throws AuthenticationException, DataFetchException {
        weatherForecastService.setUser("testdriver_franzmurner");
        weatherForecastService.setPassword("vvSS21");

        assertNotNull(weatherForecastService.getWeatherData());
    }

    @Test
    void getWeatherDataTestWrongPassword(){
        weatherForecastService.setUser("testdriver_franzmurner");
        weatherForecastService.setPassword("wrongpass");

        Throwable throwable = null;

        try {
            weatherForecastService.getWeatherData();
        } catch (DataFetchException | AuthenticationException e) {
            throwable = e;
        }

        assertNotNull(throwable);

    }
}