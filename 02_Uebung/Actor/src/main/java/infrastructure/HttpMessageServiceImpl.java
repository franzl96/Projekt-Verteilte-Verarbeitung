package infrastructure;

import application.IHttpMessageService;
import application.exceptions.EnvironmentVariablesNotFoundException;
import application.exceptions.RegistrationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import presentation.EnvironmentVariables;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class HttpMessageServiceImpl implements IHttpMessageService {

    private static final Logger LOGGER = LogManager.getLogger(HttpMessageServiceImpl.class.getName());

    public HttpMessageServiceImpl() throws EnvironmentVariablesNotFoundException {
        for(EnvironmentVariables environmentVariables: EnvironmentVariables.values()){
            if(environmentVariables.getValue() == null)
                throw new EnvironmentVariablesNotFoundException();
        }
    }

    @Override
    public void registerOnSmartHomeService() throws RegistrationException {
        try {
            String pathString = EnvironmentVariables.SMART_HOME_SERVICE_REGISTRATION_URL.getValue();
            URI uri = new URI(pathString);
            StringBuilder body = new StringBuilder()
                    .append("{\"id\":")
                    .append(EnvironmentVariables.ACTOR_ID.getValue())
                    .append(",\"actorName\":\"Shutter-Actor\",\"location\":\"Schlafzimmer\",\"serviceURL\":")
                    .append("\"http://")
                    .append(EnvironmentVariables.SERVICE_HOSTNAME.getValue())
                    .append("/api/v1/actor\"")
                    .append("}");

            String logMSG = "Body to Send: " + body;
            LOGGER.info(logMSG);

            int code = httpPOST(uri, body.toString());
            logHttpMessage(code);


        }catch (URISyntaxException e){
            throw new RegistrationException("The given url is invalid", e);
        } catch (IOException e) {
            throw new RegistrationException(e.getMessage(), e);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RegistrationException("The application is interrupted", e);
        }

    }

    private int httpPOST(URI uri, String body) throws IOException, InterruptedException {
        HttpRequest httpRequest = HttpRequest.newBuilder(uri)
                .setHeader("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .build();

        return sendRequest(httpRequest);

    }

    private int sendRequest(HttpRequest httpRequest) throws IOException, InterruptedException {
        HttpClient httpClient = HttpClient.newHttpClient();
            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            return response.statusCode();
    }

    private void logHttpMessage(int code){
        HttpStatus status = HttpStatus.resolve(code);
        String phrase = status != null ? status.getReasonPhrase() : "undefined code: " + code;
        String logMSG = "Result: " + phrase;
        LOGGER.info(logMSG);
    }


}
