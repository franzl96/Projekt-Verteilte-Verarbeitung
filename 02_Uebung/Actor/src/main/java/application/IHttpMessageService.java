package application;

import application.exceptions.RegistrationException;

public interface IHttpMessageService {

    void registerOnSmartHomeService() throws RegistrationException;
}
