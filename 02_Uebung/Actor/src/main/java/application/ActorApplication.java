package application;

import application.exceptions.EnvironmentVariablesNotFoundException;
import application.exceptions.RegistrationException;
import infrastructure.HttpMessageServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;



@SpringBootApplication
@ComponentScan(basePackages = "presentation")
public class ActorApplication {

    private static final Logger LOGGER = LogManager.getLogger(ActorApplication.class.getName());

    public static void main(String[] args) {

        try {
            IHttpMessageService httpMessageService = new HttpMessageServiceImpl();
            httpMessageService.registerOnSmartHomeService();
        } catch (EnvironmentVariablesNotFoundException e) {
            LOGGER.error("A environment variable is missing!");
            System.exit(1);
        } catch (RegistrationException e) {
            LOGGER.error(e.getMessage());
            System.exit(1);
        }

        SpringApplication.run(ActorApplication.class, args);
    }
}
