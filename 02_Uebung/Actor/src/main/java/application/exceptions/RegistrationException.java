package application.exceptions;

public class RegistrationException extends Exception{

    public RegistrationException(String msg, Throwable inner){
        super(msg, inner);
    }

    public RegistrationException(String msg){
        super(msg);
    }



}
