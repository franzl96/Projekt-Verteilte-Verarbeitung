package presentation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Locale;

public class HardwareInteraction {

    private static final Logger LOGGER = LogManager.getLogger(HardwareInteraction.class.getName());

    private String status;

    private static final HardwareInteraction instance = new HardwareInteraction();

    private HardwareInteraction(){

    }

    private void action(String status) throws NotSupportedOperationException{
        if(status.toLowerCase(Locale.ROOT).equals("open")){
            LOGGER.info("Opening Shutter...");
        }else if(status.toLowerCase(Locale.ROOT).equals("closed")){
            LOGGER.info("Close Shutter...");
        }else {
            LOGGER.error("Operation not Supported!");
            throw new NotSupportedOperationException();
        }

        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) throws NotSupportedOperationException {
        action(status);
    }

    public static HardwareInteraction getInstance() {
        return instance;
    }

}
