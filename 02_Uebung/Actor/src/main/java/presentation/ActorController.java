package presentation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ActorController {

    private static final Logger LOGGER = LogManager.getLogger(ActorController.class.getName());


    @PostMapping(value = "api/v1/actor")
    public ResponseEntity<String> setStatus(@RequestParam String status){

        String logMSG = "Received new status: " + status;
        LOGGER.info(logMSG);

        try {
            HardwareInteraction.getInstance().setStatus(status);
            return ResponseEntity.ok().build();
        } catch (NotSupportedOperationException e) {
            return ResponseEntity.badRequest().build();
        }
    }
}
