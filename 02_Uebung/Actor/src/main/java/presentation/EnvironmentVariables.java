package presentation;

public enum EnvironmentVariables {

    ACTOR_ID("ActorId"),
    SMART_HOME_SERVICE_REGISTRATION_URL("SmartHomeServiceRegistrationURL"),
    SERVICE_HOSTNAME("ServiceHostname");

    private final String name;

    EnvironmentVariables(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getValue(){
        return System.getenv(name);
    }
}
