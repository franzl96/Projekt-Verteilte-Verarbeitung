# SmartHomeService Projekt

## Was ist die Aufgabe diese Projektes
Mit diesem Projekt wurde ein eigenes Smarthomesystem auf der Basis von Rest auf dem sich N Sensoren bzw. Aktoren anschließen können gebaut.

## Aus welchen Bestandsteilen besteht das Projekt
### SmartHomeService:
Spring Boot Server der eine Restschnittstelle erstellt und die Daten der an ihn angeschlossenen Sensoren und Aktoren aufnimmt, verarbeitet und darauf reagiert wenn eine gewisse Regel an einem Server existiert.
### Aktor bzw. Actor
Der Aktor ist ein einfache Spring Boot Server der eine schnittstelle für den Service anbindet. Außerdem meldet sich dieser beim Start einmalig mit einer eindeutigen ID am SmartHomeService an
### Sensor
Der Sensor ist ein einfaches Java Programm. Der Sensor meldet sich auch beim SmartHomeService einmal an. Danach sendet er in regelmäßigen abständen Daten an den SmartHomeService
### Microsoft SQL Datenbank
Diese komponente ist eine externe komponente und kümmert sich um das persistieren und laden der Daten die vom SmartHomeService über die Rest-Schnittstelle entgegengenommen und abgefragt werden können.

**->Zusammenführung aller Dienste über Docker Compose!**

## Installations- und Bedienungsanleitung (Über eine einfache Docker-Compose Datei)
1. Laden sie die Docker Compose Datei ([Link](DockerComposeSmartHomeServiceRegistry/docker-compose.yml))) in ihr gewünschtes verzeichniss...
2. Danach führen sie in der Bash in dem von ihnen gewählten Verzeichniss einfach `docker compose up` oder alternativ `docker-compose up` auf. Achtung: Wenn sie Container aus der Registry von der Technischen Hochschule laden wollen, müssen sie am Gitlabserver dieser angemeldet sein...
3. Um nun eine bestimmte Regel hinzuzufügen, damit auch der Server auch die angemeldeten Aktoren steuert müssen sie mithilfe eines `POST` Requests (Postman, CURL, ...) folgenden Body an den Server senden
``` JSON 
{
    "sensorID" : 1, //Angemeldter Sensor
    "actorID" : 1, // Angemeldeter Actor
    "ruleName" : "TestRule", //Eindeutiger Name
    "threshold" : 4
}
```
4. Um nun die Daten zu betrachen müssen sie nur die dementsprechende Website aufrufen...
    - [Get all Sensors : http://localhost:9000/api/v1/sensors](http://localhost:9000/api/v1/sensors)
    - [Get all Actors: http://localhost:9000/api/v1/actors](http://localhost:9000/api/v1/actors)
    - [Get all Rules: http://localhost:9000/api/v1/rules](http://localhost:9000/api/v1/rules)
    - Weitere Regeln und eine Swagger Dokumentation finden sie auf [SwaggerUI](http://localhost:9000/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config)
5. Um die Dienste wieder herunterzufahren, drücken sie entweder im Docker Dashbord Stop oder  machen sie den Tastaturbefehl `ctrl + c` im laufenden Terminal Fenster.

## Installations- und Bedienungsanleitung (Mit lokalem Gradle Projekt)
1. Nachdem sie das Projekt aus dem Repository mit `git clone` geladen haben, müssen sie als erstes in das Verzeichniss 02_Uebung wechseln.
2. Im nächsten Schritt werden die benötigten JarFiles gebaut, dazu führe folgende Befehle in der Shell aus:
    ``` Bash
    ./SmartHomeService/gradlew -p SmartHomeService bootJar
    ./Actor/gradlew -p Actor bootJar
    ./Temperature_Sensor/gradlew -p Temperature_Sensor jar
    ```

3. Danach rufen sie einfach in der Bash das Kommando `docker compose up` oder alternativ `docker-compose up` auf. Damit wird dann alles gebaut, was sie zum Betrieb meines Services benötigen.
4. Um nun eine bestimmte Regel hinzuzufügen, damit auch der Server auch die angemeldeten Aktoren steuert müssen sie mithilfe eines `POST` Requests (Postman, CURL, ...) folgenden Body an den Server senden
``` JSON 
{
    "sensorID" : 1, //Angemeldter Sensor
    "actorID" : 1, // Angemeldeter Actor
    "ruleName" : "TestRule", //Eindeutiger Name
    "threshold" : 4
}
```
5. Um nun die Daten zu betrachen müssen sie nur die dementsprechende Website aufrufen...
    - [Get all Sensors : http://localhost:9000/api/v1/sensors](http://localhost:9000/api/v1/sensors)
    - [Get all Actors: http://localhost:9000/api/v1/actors](http://localhost:9000/api/v1/actors)
    - [Get all Rules: http://localhost:9000/api/v1/rules](http://localhost:9000/api/v1/rules)
    - Weitere Regeln und eine Swagger Dokumentation finden sie auf [SwaggerUI](http://localhost:9000/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config)
6. Um die Dienste wieder herunterzufahren, drücken sie entweder im Docker Dashbord Stop oder  machen sie den Tastaturbefehl `ctrl + c` im laufenden Terminal Fenster.

## Anmerkungen und Annahmen zum Projekt
* Ich habe mich dazu entschieden, bei der Anmeldung des DemoActors vom Professor die ServiceURL manuell zu vergeben, da ansonsten die Funktionsfähigkeit mit Docker Compose nich gewährleistet gewesen wär. (Dies ist fest im Quellcode verankert und muss bei Produktiveinsatz gelöscht werden, da nur Workarround!) Dies liegt daran, dass der DemoActor hardcoded localhost verwendet, aber eigentlich nur der Name des Containers oder dessen iP akzeptiert wird!! 
* Ich habe im ganzen Projekt den Namen Actor statt Aktor verwendet, weshalb es hier evtl. zu kompatibilitätsproblemen mit dem automatisierten Test kommen kann!
* Ich empfehle es tunlichest sich mit der [SwaggerUI](http://localhost:9000/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config) zu beschäftigen, um zu sehen wie die Anfragen bearbeitet werden.
* Es gibt bei dem SmartHomeService leider über Sonarcube 4 Codesmells die durch Code duplikate über die DTOs der Datenbankobjekte entstehen...
* Achtung es gibt 2 Docker Compose Files!
    * Die eine ist für die Ausführung und das Bauen Lokal auf der Maschine über die jar Dateien ([Link](02_Uebung/docker-compose.yml))
    * Die andere für das Bauen ohne Download des Projektes über die Images und die Registry von Gitlab. ([Link](DockerComposeSmartHomeServiceRegistry/docker-compose.yml))




