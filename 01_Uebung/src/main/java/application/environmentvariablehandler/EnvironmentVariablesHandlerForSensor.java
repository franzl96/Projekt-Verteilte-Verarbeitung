package application.environmentvariablehandler;

import java.util.logging.Logger;

/**
 * Ausprägung, die die Enviormentvariablen für einen Sensor bereitstellt, da braucht man bspw. keine JSON-Datei!
 */
public class EnvironmentVariablesHandlerForSensor extends AEnvironmentVariablesHandler {

    private static final  Logger LOGGER = Logger.getLogger(EnvironmentVariablesHandlerForSensor.class.getName());


    public EnvironmentVariablesHandlerForSensor(){
        super();
    }


    @Override
    public void checkVariables() throws EnviornmentVariableError{
        if(portString == null || logLevel == null || logFile == null || hostAddress == null) {
            LOGGER.info("Es sind nicht alle Environment-Variablen zum Betrieb für den Server vorhanden!!");
            LOGGER.info("Sorgen sie dafür, dass alle Variablen vorhanden sind, wie in Readme Beschrieben!");
            LOGGER.info("Beispiel: PORT=1024, LOG_LEVEL=INFO, LOGFILE=~/, HOST_ADDRESS=localhost");
            String conclusionMsg = toString();
            LOGGER.info(conclusionMsg);
            throw new EnviornmentVariableError("Es fehlt eine wichtige Umgebungsvariable!");
        }

        checkIfPathAvailable(logFile);

    }
}
