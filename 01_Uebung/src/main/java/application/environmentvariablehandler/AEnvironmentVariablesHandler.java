package application.environmentvariablehandler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Eine abstrakte Klasse, um die beiden Environmentvariablentypen darzustellen.
 */
public abstract class AEnvironmentVariablesHandler {

    String portString;
    Integer port;
    String logLevel;
    String logFile;
    String jsonFile;
    String hostAddress;

    private static final  Logger LOGGER = Logger.getLogger(AEnvironmentVariablesHandler.class.getName());

    AEnvironmentVariablesHandler(){
        try {
            setVariables();
            checkVariables();
            port = Integer.parseInt(portString);
        } catch (EnviornmentVariableError enviornmentVariableError) {
            LOGGER.log(Level.SEVERE, "" ,enviornmentVariableError);
            System.exit(1);
        }
    }

    /**
     * Prüfe ob angegebener Pfad überhaupt vorhanden.
     * @param path Pfad zu einer Datei
     * @throws EnviornmentVariableError
     */
    void checkIfPathAvailable(String path) throws EnviornmentVariableError{
        Path parentPath = Paths.get(path).getParent();

        if(Files.exists(parentPath)) return;

        createDirectory(parentPath);
    }

    /**
     * Wenn noch nicht vorhanden ist kann damit einer erstellt werden.
     * @param path
     * @throws EnviornmentVariableError
     */
    private void createDirectory(Path path) throws EnviornmentVariableError{
        try {
            Files.createDirectory(path);
        } catch (IOException e) {
            throw new EnviornmentVariableError("Fehler beim Erstellen der Directorys!");
        }
    }


    public void setVariables(){
        portString = System.getenv(String.valueOf(EnvironmentVariables.PORT));
        logLevel = System.getenv(String.valueOf(EnvironmentVariables.LOG_LEVEL));
        logFile = System.getenv(String.valueOf(EnvironmentVariables.LOG_FILE));
        jsonFile = System.getenv(String.valueOf(EnvironmentVariables.JSON_FILE));
        hostAddress = System.getenv(String.valueOf(EnvironmentVariables.HOST_ADDRESS));
    }

    /**
     * Abprüfen ob alle benötigten Environmentvariablen vorhanden sind.
     * @throws EnviornmentVariableError
     */
    public abstract void checkVariables() throws EnviornmentVariableError;


    @Override
    public String toString() {
        return "AEnvironmentVariablesHandler{" +
                "portString='" + portString + '\'' +
                ", port=" + port +
                ", logLevel='" + logLevel + '\'' +
                ", logFile='" + logFile + '\'' +
                ", jsonFile='" + jsonFile + '\'' +
                ", hostAddress='" + hostAddress + '\'' +
                '}';
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public void setLogLevel(String logLevel) {
        this.logLevel = logLevel;
    }

    public void setLogFile(String logFile) {
        this.logFile = logFile;
    }

    public void setJsonFile(String jsonFile) {
        this.jsonFile = jsonFile;
    }

    public void setHostAddress(String hostAddress) {
        this.hostAddress = hostAddress;
    }

    public Integer getPort() {
        return port;
    }

    public String getLogLevel() {
        return logLevel;
    }

    public String getLogFile() {
        return logFile;
    }

    public String getJsonFile() {
        return jsonFile;
    }

    public String getHostAddress() {
        return hostAddress;
    }
}
