package application.environmentvariablehandler;

import java.util.logging.Logger;

/**
 * Ausprägung die die EnvironmentVariablen für den Server zur verfügung stellt. Hier wird Beispielsweise kein Host gebraucht, da man selber der Host ist
 */
public class EnvironmentVariablesHandlerForServer extends AEnvironmentVariablesHandler {

    private static final  Logger LOGGER = Logger.getLogger(EnvironmentVariablesHandlerForServer.class.getName());

    public EnvironmentVariablesHandlerForServer(){
        super();
    }

    @Override
    public void checkVariables() throws EnviornmentVariableError{
        if(portString == null || logLevel == null || logFile == null || jsonFile == null) {
            LOGGER.info("Es sind nicht alle Environment-Variablen für den Betrieb des Servers vorhanden!!");
            LOGGER.info("Sorgen sie dafür, dass alle Variablen vorhanden sind, wie in Readme Beschrieben!");
            LOGGER.info("Beispiel: PORT=1024, LOG_LEVEL=INFO, LOGFILE=~/, JSON_FILE=~/");
            String conclusionMsg = toString();
            LOGGER.info(conclusionMsg);
            throw new EnviornmentVariableError("Es fehlt eine wichtige Umgebungsvariable!");
        }

        checkIfPathAvailable(logFile);
        checkIfPathAvailable(jsonFile);
    }
}
