package application;

import application.environmentvariablehandler.AEnvironmentVariablesHandler;
import application.environmentvariablehandler.EnvironmentVariablesHandlerForSensor;
import application.loggerfactory.LoggerFactory;
import domain.Measurement;
import domain.Message;
import domain.states.Symbole;
import infrastructure.MessageForwarder;
import domain.types.MeasurementType;
import domain.types.UnitType;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Ein beispielsensor zum Testen der Funktionalitäten
 */
public class TempSensor {

    private static final Logger logger = LoggerFactory.getSensorLogger(TempSensor.class.getName());
    private static final AEnvironmentVariablesHandler ENVIRONMENT_VARIABLES_HANDLER = new EnvironmentVariablesHandlerForSensor();
    private static final Integer PORT_NR = ENVIRONMENT_VARIABLES_HANDLER.getPort();
    private static final String LOCALHOST = ENVIRONMENT_VARIABLES_HANDLER.getHostAddress();


    public static void main(String[] args) {
        try {
            startSensor(LOCALHOST, PORT_NR);
        }catch (UnknownHostException unknownHostException){
            logger.log(Level.SEVERE, "Der Host konnte nicht gefunden werden!", unknownHostException);
        } catch (IOException exception) {
            logger.log(Level.SEVERE, "Es gab eine IO exception!", exception);
        }
    }

    public static void startSensor(String host, Integer port) throws IOException{
        LoggerFactory.writeEnvironmentVariablesToLog(logger);
        try(Socket socket = new Socket(host, port);
            InputStream socketInputStream = socket.getInputStream();
            OutputStream socketOutputStream = socket.getOutputStream();
            MessageForwarder messageForwarder = new MessageForwarder(socketInputStream, socketOutputStream, logger)){

            messageForwarder.sendMessage(new Message(Symbole.SENSOR_HELLO, ""));

            if(messageForwarder.receiveMessage().getType() != Symbole.STATION_HELLO) return;

            messageForwarder.sendMessage(new Message(Symbole.ACKNOWLEDGE, ""));

            if(messageForwarder.receiveMessage().getType() != Symbole.STATION_READY) return;

            Measurement measurement = new Measurement(15, UnitType.CELSIUS, MeasurementType.TEMPERATURE, LocalDateTime.of(1970, 1, 1, 0, 0, 0));

            messageForwarder.sendMessage(new Message(Symbole.MEASUREMENT, measurement.toJSONString()));

            messageForwarder.sendMessage(new Message(Symbole.TERMINATE, ""));
        }
    }

}
