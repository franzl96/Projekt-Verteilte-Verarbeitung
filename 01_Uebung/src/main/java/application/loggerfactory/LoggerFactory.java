package application.loggerfactory;

import application.environmentvariablehandler.AEnvironmentVariablesHandler;
import application.environmentvariablehandler.EnvironmentVariablesHandlerForSensor;
import application.environmentvariablehandler.EnvironmentVariablesHandlerForServer;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Wird gebraucht, um einfacher an einen Logger zu kommen, ohne diesen immer wieder neu zu konfigurieren.
 * Hier gibt es eine Server Variante, und eine Sensorvariante zum Aufrufen, da man sonst beim Prüfen der Environmentvariablen Probleme gibt
 */
public class LoggerFactory {

    private static AEnvironmentVariablesHandler environmentVariablesHandler;
    private static final Logger loggerFac = Logger.getLogger(LoggerFactory.class.getName());
    private static FileHandler fileHandler;

    private LoggerFactory(){}

    public static Logger getServerLogger(String name){
        environmentVariablesHandler = new EnvironmentVariablesHandlerForServer();
        Logger logger = Logger.getLogger(name);
        setupLogger(logger, environmentVariablesHandler.getLogFile());

        return logger;
    }


    public static Logger getSensorLogger(String name){
        environmentVariablesHandler = new EnvironmentVariablesHandlerForSensor();

        Logger logger = Logger.getLogger(name);
        setupLogger(logger, environmentVariablesHandler.getLogFile());

        return logger;
    }

    private static void setupLogger(Logger logger, String logfilePath) {
        System.setProperty("java.util.logging.SimpleFormatter.format", "%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS %4$s %2$s %5$s%6$s%n");
        setLoggerMode(logger);
        setupLogfile(logger, logfilePath);
    }

    private static void setupLogfile(Logger logger, String logFilePath){
        if(environmentVariablesHandler.getLogFile() == null) { return;}
        if(fileHandler != null) {
            logger.addHandler(fileHandler);
            return;
        }

        try {
            fileHandler = new FileHandler(logFilePath, true);
        } catch (IOException e) {
            loggerFac.log(Level.SEVERE, "Logfile path not found!", e);
            return;
        }

        fileHandler.setFormatter(new SimpleFormatter());
        logger.addHandler(fileHandler);
    }

    private static void setLoggerMode(Logger logger) {
        if(environmentVariablesHandler.getLogLevel() == null) { return;}
        switch (environmentVariablesHandler.getLogLevel()){
            case "SEVERE":
                logger.setLevel(Level.SEVERE);
                break;
            case "INFO":
                logger.setLevel(Level.INFO);
                break;
            default:
                loggerFac.severe("No valid SysVar --> no logger changes!");
        }
    }

    public static void writeEnvironmentVariablesToLog(Logger logger){

        StringBuilder environmentVariables = new StringBuilder("Environment Variables:\n")
                .append("Java Home: ")
                .append(System.getenv("JAVA_HOME"))
                .append("\n");
        environmentVariables.append("Home: ")
                .append(System.getenv("HOME"))
                .append("\n");
        environmentVariables.append("Host-Name: ")
                .append(System.getenv("HOSTNAME"))
                .append("\n");
        environmentVariables.append("Port: ")
                .append(environmentVariablesHandler.getPort())
                .append("\n");

        String logMessage = environmentVariables.toString();

        logger.info(logMessage);
    }





}
