package domain;

import com.google.gson.*;
import domain.types.MeasurementType;
import domain.types.UnitType;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Measurement implements Serializable {
    private Integer value;
    private UnitType unitType;
    private MeasurementType type;
    private LocalDateTime timestamp;

    public Measurement() {

    }

    public Measurement(Integer value, UnitType unitType, MeasurementType type, LocalDateTime timestamp) {
        this.value = value;
        this.unitType = unitType;
        this.type = type;
        this.timestamp = timestamp;
    }

    private final transient JsonSerializer<LocalDateTime> serializer = (src, typeOfSrc, context) -> {
        if(src != null){
            return new JsonPrimitive(src.format(DateTimeFormatter.ISO_DATE_TIME));
        }
        return null;
    };

    private static final transient JsonDeserializer<LocalDateTime> deserializer = (json, typeOfT, context) -> {
        if(json != null){
            return LocalDateTime.parse(json.getAsString());
        }
        return null;
    };

    public String toJSONString(){
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(LocalDateTime.class, serializer)
                .create();
        return gson.toJson(this);
    }

    public static Measurement fromJSONString(String str){
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(LocalDateTime.class, deserializer)
                .create();

        return gson.fromJson(str, Measurement.class);
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public UnitType getUnit() {
        return unitType;
    }

    public void setUnit(UnitType unitType) {
        this.unitType = unitType;
    }

    public MeasurementType getType() {
        return type;
    }

    public void setType(MeasurementType type) {
        this.type = type;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "domain.Measurement{" +
                "value=" + getValue() +
                ", unitType=" + getUnit() +
                ", type=" + getType() +
                ", timestamp=" + getTimestamp().format(DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss")) +
                '}';
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        return this.toString().equals(obj.toString());
    }
}
