package domain;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import domain.states.Symbole;

import java.io.Serializable;
import java.util.Objects;

public class Message implements Serializable {

    private Symbole type; //Das jeweilige Symbol
    private String payload; //Falls vorhanden die domain.Measurement Daten

    public Message(Symbole type, String payload){
        this.type = type;
        this.payload = payload;
    }

    public Message(String init){
        Message message = fromJSONString(init);
        if(message == null) {return;}
        type = message.type;
        payload = message.payload;
    }

    public static Message fromJSONString(String string){
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(string, Message.class);
    }

    public String toJSONString(){
        Gson gson = new GsonBuilder().create();
        return gson.toJson(this);
    }

    public Symbole getType() {
        return type;
    }

    public void setType(Symbole type) {
        this.type = type;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "Message{" +
                "type=" + type +
                ", payload='" + payload + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if (this == obj) return true;
        return this.toString().equals(obj.toString());
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, payload);
    }
}
