package domain.states;

import application.loggerfactory.LoggerFactory;

import java.util.logging.Logger;

public class ServerState {

    private final Logger logger;

    private State[][] states = {
            {
                State.WAITING_FOR_ACKNOWLEDGE,
                    State.ERROR,
                    State.ERROR,
                    State.ERROR,
                    State.TERMINATED
            },
            {
                    State.ERROR,
                    State.ERROR,
                    State.ERROR,
                    State.ERROR,
                    State.ERROR
            },
            {
                    State.ERROR,
                    State.ERROR,
                    State.ERROR,
                    State.ERROR,
                    State.ERROR
            }
            ,
            {
                    State.ERROR,
                    State.WAITING_FOR_MEASUREMENT,
                    State.ERROR,
                    State.ERROR,
                    State.TERMINATED
            }
            ,
            {
                    State.ERROR,
                    State.ERROR,
                    State.WAITING_FOR_MEASUREMENT,
                    State.ERROR,
                    State.TERMINATED
            }
            ,
            {
                    State.ERROR,
                    State.TERMINATED,
                    State.TERMINATED,
                    State.TERMINATED,
                    State.TERMINATED
            },
            {
                    State.TERMINATED,
                    State.ERROR,
                    State.ERROR,
                    State.ERROR,
                    State.TERMINATED
            }

    };

    public ServerState(Logger logger){
        this.logger = logger;
    }

    public ServerState(){
        this.logger = LoggerFactory.getServerLogger(ServerState.class.getName());
    }

    private State currentState = State.WAITING_FOR_CLIENT;

    public void transition(Symbole symbole){
        State oldState = currentState;
        currentState = states[symbole.ordinal()][currentState.ordinal()];
        String msg = oldState + " -> " +currentState;
        logger.info(msg);
    }



    public State getCurrentState() {
        return currentState;
    }
}
