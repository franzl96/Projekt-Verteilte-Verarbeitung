package domain.states;

import com.google.gson.annotations.SerializedName;

public enum Symbole {
    @SerializedName("SensorHello")
    SENSOR_HELLO("SensorHello"),

    @SerializedName("StationHello")
    STATION_HELLO("StationHello"),

    @SerializedName("StationReady")
    STATION_READY("StationReady"),

    @SerializedName("Acknowledge")
    ACKNOWLEDGE("Acknowledge"),

    @SerializedName("Measurement")
    MEASUREMENT("Measurement"),

    @SerializedName("Terminate")
    TERMINATE("Terminate"),

    @SerializedName("TerminateStation")
    TERMINATE_STATION("TerminateStation"),

    @SerializedName("Timeout")
    TIMEOUT("Timeout");

    private final String value;

    private Symbole(String value){
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
