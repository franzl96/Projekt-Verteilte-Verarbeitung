package infrastructure;

import domain.Message;

import java.io.*;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Empfängt Nachrichten und gibt sie an den Aufrufer weiter.
 * Nimmt Nachrichten vom Aufrufer an und Versendet diese.
 */
public class MessageForwarder implements Closeable {

    private final PrintStream output;
    private final Scanner input;

    private final Logger logger;

    public MessageForwarder(InputStream inputStream, OutputStream outputStream, Logger logger){
        output = new PrintStream(outputStream);
        input = new Scanner(inputStream);
        this.logger = logger;
    }

    public Message receiveMessage(){
        while (!input.hasNextLine()){
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException interruptedException) {
                logger.log(Level.SEVERE, "Unterbrechung beim empfangen von Nachrichten", interruptedException);
                Thread.currentThread().interrupt();
            }
        }

        String receivedMessage = input.nextLine();
        String msg = "Message Received: " + receivedMessage;
        logger.info(msg);
        return Message.fromJSONString(receivedMessage);
    }

    public void sendMessage(Message message){
        String messageToSend = message.toJSONString();
        String msg = "Send Message: " + messageToSend;
        logger.info(msg);
        output.println(messageToSend);
        output.flush();
    }


    @Override
    public void close() throws IOException {
        output.close();
        input.close();
    }
}
