package infrastructure;

import application.environmentvariablehandler.EnvironmentVariablesHandlerForServer;
import application.loggerfactory.LoggerFactory;
import domain.Message;
import domain.states.ServerState;
import domain.states.State;
import domain.states.Symbole;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MeasurementServer {

    private static Integer portNr;
    private static final Integer NR_THREADS = 5;

    private static final Logger logger = LoggerFactory.getServerLogger(MeasurementServer.class.getName());

    private static final EnvironmentVariablesHandlerForServer ENVIRONMENT_VARIABLES_HANDLER = new EnvironmentVariablesHandlerForServer();
    private static final String PATH_TO_JSON_FILE = ENVIRONMENT_VARIABLES_HANDLER.getJsonFile();

    private static final  ExecutorService executorService = Executors.newFixedThreadPool(NR_THREADS);
    private static final BlockingQueue<String> measurementQueue = new ArrayBlockingQueue<>(10);
    private static ServerSocket serverSocketInstance;

    private static boolean isServerRunning = false;

    public static void main(String[] args){
        setUpServer();
        startServer();
    }

    public static void setUpServer(){
        portNr = ENVIRONMENT_VARIABLES_HANDLER.getPort();
        executorService.submit(MeasurementServer::writeToJSON);
        executorService.submit(MeasurementServer::serverInterruptThread);
    }

    public static void startServer(){
        logger.log(Level.INFO, "----------------------------------------------------------");
        logger.log(Level.INFO, "----------------------------------------------------------");
        logger.log(Level.INFO, "Der Server wurde gestartet!");
        LoggerFactory.writeEnvironmentVariablesToLog(logger);
        isServerRunning = true;

        logger.info("Zum Beenden geben sie EXIT ein!");

        try (ServerSocket serverSocket = new ServerSocket(portNr);){
            while (!Thread.currentThread().isInterrupted() && !serverSocket.isClosed()) {
                serverSocketInstance = serverSocket;
                Socket client = serverSocket.accept();
                executorService.submit(() -> acceptSensor(client));
            }

        }catch (SocketTimeoutException ex) {
            logger.log(Level.INFO, "Server Timeout");
            Thread.currentThread().interrupt();
        }catch (SocketException socketException){
            logger.log(Level.SEVERE, "Socket geschlossen", socketException);
            Thread.currentThread().interrupt();
        }catch (IOException e) {
            logger.log(Level.SEVERE, "Fehler beim Starten des Servers!", e);
            Thread.currentThread().interrupt();
        }finally {
            serverStop();
        }
    }

    /**
     * Kann verwendet werden, um den Server Ordnungsgemäß herunterzufahren
     */
    public static void serverInterruptThread(){
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNext()){
            String arg = scanner.nextLine();
            if (arg.equals("EXIT")){
                serverStop();
                break;
            }
        }
    }

    public static void serverStop(){
        logger.log(Level.INFO, "Server wird heruntergefahren!");

        try {
            serverSocketInstance.close();
        } catch (IOException exception) {
            logger.log(Level.SEVERE, "Fehler beim schließen des Server Sockets!", exception);
        }

        executorService.shutdownNow();

        if(executorService.isTerminated() && serverSocketInstance.isClosed()) {
            isServerRunning = false;
        }
    }

    private static void acceptSensor(Socket client){
        try(InputStream clientInputStream = client.getInputStream();
            OutputStream clientOutputStream = client.getOutputStream();
            MessageForwarder messageForwarder = new MessageForwarder(clientInputStream, clientOutputStream, logger)) {

            ServerState serverState = new ServerState(logger);

            serverState.transition(messageForwarder.receiveMessage().getType());

            if (serverState.getCurrentState() != State.WAITING_FOR_ACKNOWLEDGE) return;

            messageForwarder.sendMessage(new Message(Symbole.STATION_HELLO, ""));

            serverState.transition(messageForwarder.receiveMessage().getType());

            if (serverState.getCurrentState() != State.WAITING_FOR_MEASUREMENT) return;

            messageForwarder.sendMessage(new Message(Symbole.STATION_READY, ""));

            while(serverState.getCurrentState() != State.ERROR &&
                    serverState.getCurrentState() != State.TERMINATED &&
                    !(Thread.currentThread().isInterrupted())){

                 Message message = messageForwarder.receiveMessage();

                 serverState.transition(message.getType());

                 if(message.getType() == Symbole.MEASUREMENT){
                     measurementQueue.put(message.getPayload());
                 }
            }

        } catch (InterruptedException interruptedException) {
            logger.log(Level.INFO, "Ein Interrupt in der Message queue ist aufgetreten!", interruptedException);
            Thread.currentThread().interrupt();
        } catch (IOException exception) {
            logger.log(Level.SEVERE, "Fehler beim Aufbauen der Verbindung zum Sensor", exception);
            Thread.currentThread().interrupt();
        }
    }

    public static void writeToJSON(){
        try (
                BufferedWriter bufferedWriter = Files.newBufferedWriter(Path.of(PATH_TO_JSON_FILE), StandardOpenOption.APPEND, StandardOpenOption.CREATE);
        ){
            while (!Thread.currentThread().isInterrupted()){
                saveMeasurement(bufferedWriter, measurementQueue.take());
            }
        } catch (InterruptedException interruptedException){
            logger.log(Level.SEVERE, "Interrupt Blocking Queue für Schreiben von Daten! -> Beim ausschalten des Server ist dies Unkritisch", interruptedException);
            Thread.currentThread().interrupt();
        } catch (IOException exception) {
            logger.log(Level.SEVERE, "Daten konnten nicht in die Datei geschrieben werden! -> Beim ausschalten des Server ist dies Unkritisch", exception);
            Thread.currentThread().interrupt();
        }
    }


    private static void saveMeasurement(BufferedWriter bufferedWriter, String measurement) throws IOException{
        bufferedWriter.write(measurement + "\n");
        bufferedWriter.flush();
    }

    public static boolean isServerRunning(){
        return isServerRunning;
    }

    /**
     * Zugriff auf Queue für Test!
     * @return
     */
    public static BlockingQueue<String> getMeasurementQueue() {
        return measurementQueue;
    }
}
