package domain;

import TestHelper.RemoveArtifacts;
import domain.states.Symbole;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MessageTest {

    Message message = new Message(Symbole.STATION_READY, "{Hallo}");

    @Test
    void testingSerialisation(){
        String jsonString = message.toJSONString();

        Message decodedMessage = Message.fromJSONString(jsonString);

        assertEquals(message.getType(), decodedMessage.getType());
        assertEquals(message.getPayload(), decodedMessage.getPayload());
    }

    @Test
    void testingSerialisationOverInit(){
        String messageJSON = message.toJSONString();
        Message decodedMessage = new Message(messageJSON);

        assertEquals(message.getType(), decodedMessage.getType());
        assertEquals(message.getPayload(), decodedMessage.getPayload());

        
    }

    @Test
    void toStringTest(){
        assertEquals("{\"type\":\"StationReady\",\"payload\":\"{Hallo}\"}", message.toJSONString());
    }

    @Test
    void testEquals(){
        Message anotherEqualMessage = new Message(Symbole.STATION_READY, "{Hallo}");
        Message anotherNoEqualMessage = new Message(Symbole.STATION_READY, "{ollaH}");

        assertEquals(anotherEqualMessage, message);
        assertNotEquals(anotherNoEqualMessage, message);

        assertEquals(message, message);

    }

    @AfterAll
    static void tearDown(){
        RemoveArtifacts.removeAll();
    }


}