package domain.states;

import TestHelper.RemoveArtifacts;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ServerStateTest {

    @Test
    void fromWaitingToTerminated() {

        ServerState serverState = new ServerState();

        assertEquals(State.WAITING_FOR_CLIENT, serverState.getCurrentState());

        serverState.transition(Symbole.SENSOR_HELLO);
        assertEquals(State.WAITING_FOR_ACKNOWLEDGE, serverState.getCurrentState());

        serverState.transition(Symbole.ACKNOWLEDGE);
        assertEquals(State.WAITING_FOR_MEASUREMENT, serverState.getCurrentState());

        serverState.transition(Symbole.MEASUREMENT);
        assertEquals(State.WAITING_FOR_MEASUREMENT, serverState.getCurrentState());

        serverState.transition(Symbole.TERMINATE);
        assertEquals(State.TERMINATED, serverState.getCurrentState());

        serverState.transition(Symbole.MEASUREMENT);
        assertEquals(State.TERMINATED, serverState.getCurrentState());

        serverState.transition(Symbole.ACKNOWLEDGE);
        assertEquals(State.TERMINATED, serverState.getCurrentState());

        serverState.transition(Symbole.MEASUREMENT);
        assertEquals(State.TERMINATED, serverState.getCurrentState());

        serverState.transition(Symbole.TERMINATE);
        assertEquals(State.TERMINATED, serverState.getCurrentState());
    }

    @Test
    void fromWaitingToWaitingToTerminated() {

        ServerState serverState = new ServerState();

        assertEquals(State.WAITING_FOR_CLIENT, serverState.getCurrentState());

        serverState.transition(Symbole.SENSOR_HELLO);
        assertEquals(State.WAITING_FOR_ACKNOWLEDGE, serverState.getCurrentState());

        serverState.transition(Symbole.TERMINATE);
        assertEquals(State.TERMINATED, serverState.getCurrentState());

    }

    @Test
    void fromWaitingToErrorToTerminated() {

        ServerState serverStateOverTerminate = new ServerState();
        ServerState serverStateOverMeasurement = new ServerState();
        ServerState serverStateOverAcknowledge = new ServerState();


        assertEquals(State.WAITING_FOR_CLIENT, serverStateOverTerminate.getCurrentState());
        assertEquals(State.WAITING_FOR_CLIENT, serverStateOverMeasurement.getCurrentState());
        assertEquals(State.WAITING_FOR_CLIENT, serverStateOverAcknowledge.getCurrentState());

        serverStateOverTerminate.transition(Symbole.TERMINATE);
        assertEquals(State.ERROR, serverStateOverTerminate.getCurrentState());

        serverStateOverMeasurement.transition(Symbole.MEASUREMENT);
        assertEquals(State.ERROR, serverStateOverMeasurement.getCurrentState());

        serverStateOverAcknowledge.transition(Symbole.ACKNOWLEDGE);
        assertEquals(State.ERROR, serverStateOverAcknowledge.getCurrentState());

        //On State Error With next Symbols
        serverStateOverTerminate.transition(Symbole.SENSOR_HELLO);
        assertEquals(State.ERROR, serverStateOverTerminate.getCurrentState());

        serverStateOverTerminate.transition(Symbole.MEASUREMENT);
        assertEquals(State.ERROR, serverStateOverTerminate.getCurrentState());

        serverStateOverTerminate.transition(Symbole.ACKNOWLEDGE);
        assertEquals(State.ERROR, serverStateOverTerminate.getCurrentState());

        //From ErrorTo Terminate
        serverStateOverTerminate.transition(Symbole.TERMINATE);
        assertEquals(State.TERMINATED, serverStateOverTerminate.getCurrentState());


    }

    @Test
    void fromWaitingToWaitingToError() {

        ServerState serverStateHello = new ServerState();
        ServerState serverStateMeasurement = new ServerState();


        assertEquals(State.WAITING_FOR_CLIENT, serverStateHello.getCurrentState());
        assertEquals(State.WAITING_FOR_CLIENT, serverStateMeasurement.getCurrentState());


        serverStateHello.transition(Symbole.SENSOR_HELLO);
        assertEquals(State.WAITING_FOR_ACKNOWLEDGE, serverStateHello.getCurrentState());

        serverStateMeasurement.transition(Symbole.SENSOR_HELLO);
        assertEquals(State.WAITING_FOR_ACKNOWLEDGE, serverStateMeasurement.getCurrentState());


        serverStateHello.transition(Symbole.SENSOR_HELLO);
        assertEquals(State.ERROR, serverStateHello.getCurrentState());

        serverStateMeasurement.transition(Symbole.MEASUREMENT);
        assertEquals(State.ERROR, serverStateMeasurement.getCurrentState());

    }

    @Test
    void fromWaitingToWaitingtoAcknToError() {

        ServerState serverStateHello = new ServerState();
        ServerState serverStateAcknowledge = new ServerState();


        assertEquals(State.WAITING_FOR_CLIENT, serverStateHello.getCurrentState());
        assertEquals(State.WAITING_FOR_CLIENT, serverStateAcknowledge.getCurrentState());


        serverStateHello.transition(Symbole.SENSOR_HELLO);
        assertEquals(State.WAITING_FOR_ACKNOWLEDGE, serverStateHello.getCurrentState());

        serverStateAcknowledge.transition(Symbole.SENSOR_HELLO);
        assertEquals(State.WAITING_FOR_ACKNOWLEDGE, serverStateAcknowledge.getCurrentState());

        serverStateHello.transition(Symbole.ACKNOWLEDGE);
        assertEquals(State.WAITING_FOR_MEASUREMENT, serverStateHello.getCurrentState());

        serverStateAcknowledge.transition(Symbole.ACKNOWLEDGE);
        assertEquals(State.WAITING_FOR_MEASUREMENT, serverStateAcknowledge.getCurrentState());


        serverStateHello.transition(Symbole.SENSOR_HELLO);
        assertEquals(State.ERROR, serverStateHello.getCurrentState());

        serverStateAcknowledge.transition(Symbole.ACKNOWLEDGE);
        assertEquals(State.ERROR, serverStateAcknowledge.getCurrentState());

    }


    @AfterAll
    static void tearDown(){
        RemoveArtifacts.removeAll();
    }


}