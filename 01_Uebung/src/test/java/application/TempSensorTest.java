package application;

import TestHelper.RemoveArtifacts;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.UnknownHostException;

import static org.junit.jupiter.api.Assertions.*;

class TempSensorTest {

    @Test
    void unknownHost(){
        try {
            TempSensor.startSensor("No Host", 13134);
        }catch (UnknownHostException exception){
            assertEquals(UnknownHostException.class, exception.getClass());
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    @AfterAll
    static void tearDown(){
        RemoveArtifacts.removeAll();
    }



}