package application.environmentvariablehandler;

import TestHelper.RemoveArtifacts;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EnvironmentVariablesHandlerForServerTest {


    @Test
    void environmentVariablesReachable(){
        EnvironmentVariablesHandlerForServer environmentVariablesHandlerForServer = new EnvironmentVariablesHandlerForServer();

        assertEquals(1024, environmentVariablesHandlerForServer.getPort());
        assertEquals("INFO", environmentVariablesHandlerForServer.getLogLevel());
        assertEquals("build/testartifacts/logfile.log", environmentVariablesHandlerForServer.getLogFile());
        assertEquals("build/testartifacts/measurements.json", environmentVariablesHandlerForServer.getJsonFile());
    }


    @Test
    void testWithoutEnviromentVariables(){
        EnvironmentVariablesHandlerForServer environmentVariablesHandlerForServer = new EnvironmentVariablesHandlerForServer();
        environmentVariablesHandlerForServer.setJsonFile(null);
        environmentVariablesHandlerForServer.setLogFile(null);
        environmentVariablesHandlerForServer.setPort(null);
        environmentVariablesHandlerForServer.setLogLevel(null);

        try{
            environmentVariablesHandlerForServer.checkVariables();
            fail("Keine Exception geworfen!");
        }catch (EnviornmentVariableError enviornmentVariableError){
            assertEquals("application.environmentvariablehandler.EnviornmentVariableError: Es fehlt eine wichtige Umgebungsvariable!" ,enviornmentVariableError.toString());
        }
    }

    @AfterAll
    static void tearDown(){
        RemoveArtifacts.removeAll();
    }



}