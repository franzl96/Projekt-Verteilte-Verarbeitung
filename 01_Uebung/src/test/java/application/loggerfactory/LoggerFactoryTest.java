package application.loggerfactory;

import TestHelper.RemoveArtifacts;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;


import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.*;

class LoggerFactoryTest {

    @Test
    void getNormalLogger(){

        Logger logger = LoggerFactory.getServerLogger("TestLogger");
        assertEquals("INFO", logger.getLevel().getName());

    }


    @AfterAll
    static void tearDown(){
        RemoveArtifacts.removeAll();
    }






}