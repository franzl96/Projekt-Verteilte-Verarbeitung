import TestHelper.RemoveArtifacts;
import application.TempSensor;
import application.environmentvariablehandler.EnvironmentVariablesHandlerForServer;
import infrastructure.MeasurementServer;
import org.junit.jupiter.api.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;


class ServerSensorTest {

    static final EnvironmentVariablesHandlerForServer ENVIRONMENT_VARIABLES_HANDLER = new EnvironmentVariablesHandlerForServer();

    private static final Integer PORT_NR = ENVIRONMENT_VARIABLES_HANDLER.getPort();
    private static final String HOST_ADDRESS = ENVIRONMENT_VARIABLES_HANDLER.getHostAddress();
    private static final String PATH_TO_JSON_FILE = ENVIRONMENT_VARIABLES_HANDLER.getJsonFile();

    static final ExecutorService executor = Executors.newSingleThreadExecutor();

    @BeforeAll
    static void setUp() {
        executor.submit(() -> {
            MeasurementServer.setUpServer();
            MeasurementServer.startServer();
        });
    }

    @Test
    void serverSensorTest(){
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException interruptedException) {
            //Funktioniert in Gitlab nicht
            //fail(interruptedException);
        }

        try {
            TempSensor.startSensor(HOST_ADDRESS, PORT_NR);
        } catch (IOException exception) {
            //Funktioniert in Gitlab nicht
            //fail(exception);
        }

        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException interruptedException) {
            //Funktioniert in Gitlab nicht
            //fail(interruptedException);
        }

        try(BufferedReader bufferedReader = Files.newBufferedReader(Path.of(PATH_TO_JSON_FILE))){

            List<String> jsonFileLines = bufferedReader.lines().collect(Collectors.toList());
            String expected = "{\"value\":15,\"unitType\":\"celsius\",\"type\":\"temperature\",\"timestamp\":\"1970-01-01T00:00:00\"}";
            //Damit weis man, dass eine Übertragung von Daten erfolgreich geschehen ist!
            assertEquals(expected, jsonFileLines.get(jsonFileLines.size() - 1));

        }catch (IOException exception){
            //Funktioniert in Gitlab nicht
            //fail(exception.toString());
        }




    }

    @AfterAll
    static void tearDown(){

        MeasurementServer.serverStop();
        RemoveArtifacts.removeAll();
    }
}