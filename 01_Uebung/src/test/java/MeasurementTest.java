import TestHelper.RemoveArtifacts;
import domain.Measurement;
import org.junit.jupiter.api.AfterAll;
import domain.types.MeasurementType;
import domain.types.UnitType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class MeasurementTest {

    static Measurement measurement;

    @BeforeAll
    static void setup(){
        measurement = new Measurement(42, UnitType.CELSIUS, MeasurementType.TEMPERATURE, LocalDateTime.of(1970, 1, 1, 0, 0 ,0));
    }

    @Test
    @DisplayName("Create measurement string")
    void encodeMeasurementString() throws IOException {
        String expected = "{\"value\":42,\"unitType\":\"celsius\",\"type\":\"temperature\",\"timestamp\":\"1970-01-01T00:00:00\"}";
        assertEquals(expected, measurement.toJSONString());
    }

    @Test
    void decodeMeasurementString() throws IOException{
        String encodedMeasurement = measurement.toJSONString();
        Measurement decodedMeasurement = Measurement.fromJSONString(encodedMeasurement);

        assertEquals(measurement.toString(), decodedMeasurement.toString());
    }

    @Test
    void equalsTest(){
        assertEquals(measurement, measurement);
        assertNotEquals(null, measurement);
    }

    @AfterAll
    static void tearDown(){
        RemoveArtifacts.removeAll();
    }




}