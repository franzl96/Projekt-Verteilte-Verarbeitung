package TestHelper;

import application.environmentvariablehandler.EnvironmentVariablesHandlerForServer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.fail;

public class RemoveArtifacts {

    /**
     * Sollen die Daten nach dem Test gelöscht werden?
     */
    static final boolean turnOffRemove = false;

    static final EnvironmentVariablesHandlerForServer ENVIRONMENT_VARIABLES_HANDLER = new EnvironmentVariablesHandlerForServer();
    static final Path pathToArtifactFolder = Paths.get(ENVIRONMENT_VARIABLES_HANDLER.getLogFile()).getParent();

    synchronized public static void removeAll(){
        if(turnOffRemove) return;

        try {
            Files.deleteIfExists(Path.of(ENVIRONMENT_VARIABLES_HANDLER.getLogFile()));
            Files.deleteIfExists(Path.of(ENVIRONMENT_VARIABLES_HANDLER.getJsonFile()));
            //Files.deleteIfExists(pathToArtifactFolder);
        } catch (IOException exception) {
            fail("Exception beim Löschen der Dateien!");
        }
    }



}
