package infrastructure;

import TestHelper.RemoveArtifacts;
import application.environmentvariablehandler.EnvironmentVariablesHandlerForServer;
import domain.Measurement;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import domain.types.MeasurementType;
import domain.types.UnitType;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class MeasurementServerTest {

    Logger logger = Logger.getLogger("TestMeasurementServer");


    static final EnvironmentVariablesHandlerForServer ENVIRONMENT_VARIABLES_HANDLER = new EnvironmentVariablesHandlerForServer();

    static final String pathToTestJSON = ENVIRONMENT_VARIABLES_HANDLER.getJsonFile();

    static ExecutorService executor = Executors.newSingleThreadExecutor();


    @Test
    void serverStartStop(){

        executor.submit(()->{
            MeasurementServer.setUpServer();
            MeasurementServer.startServer();
        });


        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException interruptedException) {
            logger.log(Level.SEVERE, "", interruptedException);
        }

        MeasurementServer.serverStop();

        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException interruptedException) {
            logger.log(Level.SEVERE, "", interruptedException);
            System.exit(0);
        }

        assertFalse(MeasurementServer.isServerRunning());
    }

    @Test
    void writeQueuedToJSONTest() {


        ExecutorService executorService = Executors.newSingleThreadExecutor();

        executorService.submit(MeasurementServer::writeToJSON);

        try {
            Measurement measurement = new Measurement(42, UnitType.CELSIUS, MeasurementType.TEMPERATURE, LocalDateTime.of(1970, 1, 1, 0, 0, 0));
            MeasurementServer.getMeasurementQueue().put(measurement.toJSONString());

            TimeUnit.SECONDS.sleep(1);
        }catch (InterruptedException interruptedException) {
            //INTERRUPT wegen Queue zu erwarten
        }


        try(BufferedReader bufferedReader = Files.newBufferedReader(Path.of(pathToTestJSON))){

            List<String> jsonFileLines = bufferedReader.lines().collect(Collectors.toList());
            String expected = "{\"value\":42,\"unitType\":\"celsius\",\"type\":\"temperature\",\"timestamp\":\"1970-01-01T00:00:00\"}";

            assertEquals(expected, jsonFileLines.get(jsonFileLines.size() - 1));


        }catch (IOException exception){
            fail(exception.toString());
        }


    }

    @AfterAll
    static void tearDown(){
        RemoveArtifacts.removeAll();
    }










}