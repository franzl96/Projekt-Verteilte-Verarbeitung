# Measurement Server der TH-Rosenheim

## Was ist die Aufgabe des Servers
Der Measurement Server läuft im Hintergrund und verbindet sich mit Sensoren, die die Sprache des Servers unterstützen.\
Wenn der Server Daten von einem Sensor erhält die auch korrekt sind, dann werden diese Daten in eine JSON Datei geschrieben.
Damit kann man ganz einfach Beispielsweise einen Temperatursensor o.ä. verbinden und so die Daten tracken und evtl. mit einem anderen Programm wieder weiterverarbeiten.
## Wie funktioniert der Server prinzipiell
Der Server Startet und geht in eine endlosschleife, die durch eine einfache Eingabe in die Commandline beendet werden kann. Verbindet sich nun ein Sensor mit dem Server, dann wird ein neuer Thread erstellt in der die Verbindung mit dem Sensor verwaltet wird, bis sich der Sensor wieder Terminiert.
Alle Empfangenen Measurement Daten werden über eine Blocking Queue an einen Schreibe Thread weitergeleitet und dann in einer JSON-Datei persistiert

## Installations- und Bedienungsanleitung
* Das Projekt lässt sich ganz einfach über den Befehl `git clone https://inf-git.fh-rosenheim.de/vv-inf-sose21/murnerfranz.git` klonen
* Öffenen sie das Projekt mit ihrer IDE die Gradle unterstützt und dann wird alles von dem Gradle Build Tool vorbereitet.
* Um nun den Server zu verwenden, müssen sie folgende Umgebungsvariablen setzten
    * PORT
        * Festlegbar, damit man evtl. auf schon vorhandene Ports reagieren kann
    * LOG_LEVEL
        * Loglevel bestimmt, ob nur die schwerwigenden Fehler (`SEVERE`) ausgegeben werden, oder ob auch info (`INFO`) Fehler ausgegeben werden.
    * LOG_FILE 
        * Der Pfad muss vollständig mit Dateinamen angegeben werden! Geht das Verzeichnis ab, wird dieses Automatisch erstellt.
    * JSON_FILE 
        * Der Pfad muss vollständig mit Dateinamen angegeben werden! Geht das Verzeichnis ab, wird dieses Automatisch erstellt.
* Sollten sie keine Variablen setzen, werden sie eine Fehlermeldung bekommen, da alle Umgebungsvariablen benötigt werden, um eine saubere verarbeitung zu gewährleisten.
* Für den Betrieb über den Run Button müssen wahrscheinlich die Umgebungsvariablen in der Config des Runs hinzugefügt werden.
  `PORT=3000;LOG_LEVEL=INFO;LOG_FILE=/Users/franzmurner/logfile.log;JSON_FILE=/Users/franzmurner/measurements.json;HOST_ADDRESS=localhost`
* Den Server Starten sie dann ganz einfach über den Run Button in ihrer IDE
* Um den Server wieder herunterzufahren, müssen sie nur EXIT eingeben.

* Wenn sie nun den Sensor auch noch ausprobieren wollen, müssen sie zusätzlich noch die Hostadresse als Umgebungsvariable angeben. Im lokalen Fall wär dies `localhost`.

## Installation über Docker - empfohlen

Noch einfacher geht die Verwendung über Docker, dazu müssen sie nur den folgenden Befehl verwenden. \
`docker run -it -p <Ausgangsport>:1024 -v <Speicherort für Daten>:/Data  inf-docker.fh-rosenheim.de/vv-inf-sose21/murnerfranz` 
* Denken Sie daran, dass der Ausgangsport beim Sensor Identisch sein muss!

**Beispiel:** \
`docker run -it -p 3000:1024 -v ~/:/Data  inf-docker.fh-rosenheim.de/vv-inf-sose21/murnerfranz`

**Eigene Environmentvariablen:** \
Wenn sie ihre eigenen Environmentvariablen verwenden wollen, dann können sie die die aus dem Dockerfile einfach mit dem Befehl `--env` und dahinter die Environmentvariable überschreiben.

**Beispiel:**\
`docker run -it -p 3000:6000 --env PORT=6000 --env LOG_LEVEL="INFO" --env LOG_FILE="/Data/logfile.log" --env JSON_FILE="/Data/measurements.json" --env HOST_ADDRESS="localhost" -v ~/:/Data inf-docker.fh-rosenheim.de/vv-inf-sose21/murnerfranz`



Zum Beenden müssen sie auch hier nur wieder EXIT in die Kommandozeile eingeben, oder einfach den Container beenden.

## Für Fortgeschrittene
Wenn sie ihren eigenen Sensor Programmieren wollen, dann finden sie ein Beispiel in Form eines TempSensors im Repository.
